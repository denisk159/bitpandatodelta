package de.kiesel.bitpanda;

import de.kiesel.bitpanda.bitpandaApi.enums.AttributesType;
import de.kiesel.bitpanda.services.BscscanService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BscScanTest {

  @Autowired
  private BscscanService bscscanService;

  @Test
  void testBnbToSafemoon() {
    var data = bscscanService.getTransactionInfos("0x53b6a043eda92a9c97d443a61c597a1a9ddaa8253ceeac704136545594191cd0");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("BNB", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(2.182925, data.getAmountIn());
    assertEquals("SAFEMOON", data.getCurrencyOut());
    assertEquals(8456220399.784752415, data.getAmountOut());
    assertEquals("2021-03-20 12:05:55", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("BNB", data.getCurrencyFee());
    assertEquals(0.00326514, data.getAmountFee());
    assertEquals(264.48, data.getPriceCurrencyIn());
  }

  @Test
  void testAprovements() {
    var data = bscscanService.getTransactionInfos("0xe7996047cacf3e1b28e67b12c3bd9d9f2d775647becaa5234baa52e9a46107ea");
  }

  @Test
  void testTransferOut() {
    var data = bscscanService.getTransactionInfos("0x179d03f8d97d6c1af261121bffd176617edf4242968792fbade60c17383396f4");
    assertEquals(AttributesType.TRANSFER.getName(), data.getType());
    assertEquals(33.002d, data.getAmountOut());
    assertEquals(33.002d, data.getAmountIn());
    assertEquals(0.000356115d, data.getAmountFee());
    assertEquals("BNB", data.getCurrencyOut());
    assertEquals("BNB", data.getCurrencyFee());
  }

  @Test
  void testFailures1() {
    List<String> failedHashes = new ArrayList<>();
    failedHashes.add("0x8a57bc808273a91765402382a65833372d5a84d9f3a7bcec35daed6347ddd85f");
    failedHashes.add("0xf78fa06b89cec771090b3b5cda83747793301808767fb420b7d6269574a34ee6");
    failedHashes.add("0x8196382a0cd9fa995c6f2c9a8f6b7f3c17bb85038a207351062652289ea256cb");
    failedHashes.add("0xe15bcc247f904579d80771a3a45dfa138dda46f142610c1e984ea2484f7f2eb4");
    failedHashes.add("0x07a89b2599ac69cf76bdc76ed42080f26c9aa327d59d1d3b32dbc9ebdd945847");
    failedHashes.add("0x5dd05463dd7ab246f33695c1be6e202529c150f028fa8cf7c7c1c622eec402aa");
    for (String hashToFail : failedHashes) {
      failedTransaction(hashToFail);
    }
  }

  @Test
  void testSell() {
    var data = bscscanService.getTransactionInfos("0xa6ad351a0543adf1418b6ed0a92bbd6009e7b80ca6016bf90d6830d0e002b108");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("SAFEMOON", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(3600000000.0, data.getAmountIn());
    assertEquals("BNB", data.getCurrencyOut());
    assertEquals(47.45728489517413411, data.getAmountOut());
    assertEquals("2021-04-21 04:02:38", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("BNB", data.getCurrencyFee());
    assertEquals(0.00240291, data.getAmountFee());
    assertEquals(543.71, data.getPriceCurrencyIn());
  }

  @Test
  void testMulti() {
    //Weiteres Beispiel https://bscscan.com/tx/0x2810f484fbae454b68482fa6df16138acf94770e0d3789fb166da5d9a51f9ff8
    var data = bscscanService.getTransactionInfos("0xe4c46ecf500716591541822fb26ada84e40d60c3bf570b5416e91b2bcde4a2aa");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("BNB", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(0.7, data.getAmountIn());
    assertEquals("CHAR", data.getCurrencyOut());
    assertEquals(79238.14722601738, data.getAmountOut());
    assertEquals("2021-04-21 04:51:32", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("BNB", data.getCurrencyFee());
    assertEquals(0.001213945, data.getAmountFee());
    assertEquals(543.71, data.getPriceCurrencyIn());
  }

  @Test
  void testXYZ() {
    var data = bscscanService.getTransactionInfos("0xcaa88c2f9475af17dc88ccd8af0df0a70ad401e7c8e6a0d1874df5ba598fbaf5");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("BNB", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(0.35, data.getAmountIn());
    assertEquals("MNCH", data.getCurrencyOut());
    assertEquals(2597940.17255618, data.getAmountOut());
    assertEquals("2021-03-30 11:08:36", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("BNB", data.getCurrencyFee());
    assertEquals(0.00169025, data.getAmountFee());
    assertEquals(311.54, data.getPriceCurrencyIn());
  }

  void failedTransaction(String hash) {
    var data = bscscanService.getTransactionInfos(hash);
    Assert.isNull(data, "Transaktion fehlerhaft. Sollte deshalb <null> sein.");
  }

}

package de.kiesel.bitpanda;

import de.kiesel.bitpanda.services.FiFoCalculator;
import de.kiesel.bitpanda.services.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FiFoTest {

  @Autowired
  TransactionService transactionService;

  @Autowired
  FiFoCalculator fiFoCalculator;

  @Test
  void testBsc2() {
    var transactionList = transactionService.getTransactionsOfDepot(2);
    fiFoCalculator.doCalculate(transactionList);
  }

  @Test
  void testEther2() {
    var transactionList = transactionService.getTransactionsOfDepot(3);
    fiFoCalculator.doCalculate(transactionList);
  }

  @Test
  void testBsc1() {
    var transactionList = transactionService.getTransactionsOfDepot(2);
    fiFoCalculator.doCalculate(transactionList);
  }

  @Test
  void testEther1() {
    var transactionList = transactionService.getTransactionsOfDepot(3);
    fiFoCalculator.doCalculate(transactionList);
  }

}

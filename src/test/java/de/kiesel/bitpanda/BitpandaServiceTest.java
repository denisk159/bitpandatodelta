package de.kiesel.bitpanda;

import de.kiesel.bitpanda.enums.DepotType;
import de.kiesel.bitpanda.services.BitpandaService;
import de.kiesel.bitpanda.services.DepotService;
import de.kiesel.bitpanda.services.ProfileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BitpandaServiceTest {

  @Autowired
  private BitpandaService bitpandaService;

  @Autowired
  private ProfileService profileService;

  @Autowired
  private DepotService depotService;

  @Test
  void testDenis() {
    var profile = profileService.getByIdentifier("denis");
    var depot = depotService.getBy(profile.getId(), DepotType.BITPANDA);
    bitpandaService.checkCryptos(depot);
  }

}

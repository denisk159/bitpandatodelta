package de.kiesel.bitpanda;

import de.kiesel.bitpanda.bitpandaApi.enums.AttributesType;
import de.kiesel.bitpanda.services.EtherscanService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
class EtherScanTest {

  @Autowired
  private EtherscanService etherscanService;

  @Test
  void testDenis() {
    etherscanService.getTransactionInfos("0x33b341b7b729029bd9d0356aae73430a0c13683fa2332541a9b999bcb521d15a");
    etherscanService.getTransactionInfos("0x92a472f494b85f9d787de368edb70cea0a29c43be2476e7876dbd8c8f1f6cbf9");
  }

  @Test
  void testEtheriumToPoodle() {
    var data = etherscanService.getTransactionInfos("0x20359a296ecfd761fa46ec76b361aa406476345aa4c48a98ddc913c17516812c");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("ETH", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(0.78, data.getAmountIn());
    assertEquals("POODL", data.getCurrencyOut());
    assertEquals(8.345567429874849E9, data.getAmountOut());
    assertEquals("2021-03-22 12:04:54", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("ETH", data.getCurrencyFee());
    assertEquals(0.022810941, data.getAmountFee());
    assertEquals(1682.05, data.getPriceCurrencyIn());
    assertEquals("0x20359a296ecfd761fa46ec76b361aa406476345aa4c48a98ddc913c17516812c", data.getHash());
  }

  @Test
  void testEtheriumToNsure() {
    var data = etherscanService.getTransactionInfos("0xd88f02f879cbfe0832e8a0b06a142ee8b217d041215f3a4f6b03b2aba983b92c");
    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("Nsure", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(624.6688608119188, data.getAmountIn());
    assertEquals("ETH", data.getCurrencyOut());
    assertEquals(0.2705700793527356, data.getAmountOut());
    assertEquals("2021-03-28 15:32:39", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("ETH", data.getCurrencyFee());
    assertEquals(0.020044824, data.getAmountFee());
    assertEquals(1686.74, data.getPriceCurrencyIn());
    assertEquals("0xd88f02f879cbfe0832e8a0b06a142ee8b217d041215f3a4f6b03b2aba983b92c", data.getHash());
  }

  @Test
  void testBinanceToMetamask() {
    var data = etherscanService.getTransactionInfos("0x3c0db34c3f14af64198b281e0f5b375840c0eccd39f7c61c56731a4fd749f390");

    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertNull(data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertNull(data.getAmountIn());
    assertEquals("ETH", data.getCurrencyOut());
    assertEquals(0.29641, data.getAmountOut());
    assertEquals("2021-05-14 22:44:54", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("ETH", data.getCurrencyFee());
    assertEquals(0.003759, data.getAmountFee());
    assertEquals(4087.02, data.getPriceCurrencyIn());
  }

  @Test
  void testBuySpaceHoge() {
    var data = etherscanService.getTransactionInfos("0x33b341b7b729029bd9d0356aae73430a0c13683fa2332541a9b999bcb521d15a");

    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("ETH", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(0.08999991903019201, data.getAmountIn());
    assertEquals("SOGE", data.getCurrencyOut());
    assertEquals(8.0725585E9, data.getAmountOut());
    assertEquals("2021-05-19 19:47:18", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("ETH", data.getCurrencyFee());
    assertEquals(0.025154697, data.getAmountFee());
    assertEquals(2442.89, data.getPriceCurrencyIn());
  }

  @Test
  void testBuyCoshi() {
    var data = etherscanService.getTransactionInfos("0x92a472f494b85f9d787de368edb70cea0a29c43be2476e7876dbd8c8f1f6cbf9");

    assertEquals(AttributesType.BUY.getName(), data.getType());
    assertEquals("ETH", data.getCurrencyIn());
    //AmountIn is with substracted fees
    assertEquals(0.08999976432547066, data.getAmountIn());
    assertEquals("CoShi", data.getCurrencyOut());
    assertEquals(1.12174E10, data.getAmountOut());
    assertEquals("2021-05-19 19:52:28", data.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

    assertEquals("ETH", data.getCurrencyFee());
    assertEquals(0.021967219, data.getAmountFee());
    assertEquals(2442.89, data.getPriceCurrencyIn());
  }

}

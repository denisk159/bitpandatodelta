package de.kiesel.bitpanda.telegram;

import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class TelegramBot extends TelegramLongPollingBot {

  TelegramConfiguration configuration;

  TelegramBot(TelegramConfiguration configuration) {
    this.configuration = configuration;
  }

  @Override
  public void onUpdateReceived(Update update) {
    if (update.hasMessage() && update.getMessage().hasText()) {
      String message = update.getMessage().getText();
      var telegramCommand = TelegramCommands.getByCommand(message);
      if (telegramCommand != null) {
        switch (telegramCommand) {
          case LOGS:
            printLogs(update);
            break;
          case HELP:
            printCommands(update);
            break;
          case NEW:
            printNew(update);
        }
      }
    }
  }

  private void printNew(Update update) {
    var text = new StringBuilder();
    //for (String entry : CoinTracker.lastFoundProducts.values()) {
    // text.append(entry).append("\n");
    //}
    if (text.length() == 0) {
      text.append("Keine Produkte vorhanden.");
    }
    sendMessage(update, text.toString());
  }

  private void printLogs(Update update) {
    var text = new StringBuilder();
    //  for (LogEntry entry : CoinTracker.logs) {
    //    text.append(DateFormatUtils.format(entry.date, "yyyy-MM-dd HH:mm:ss")).append("| ").append(entry.type).append(": ").append(entry.errorMessage).append("\n");
    // }
    if (text.length() == 0) {
      text.append("Keine Logs vorhanden.");
    }
    sendMessage(update, text.toString());
  }

  private void printCommands(Update update) {
    var text = new StringBuilder("Verfügbare Befehle");
    for (TelegramCommands command : TelegramCommands.values()) {
      text.append("\n" + " - ").append(command.getCommand()).append(": ").append(command.getDescription());
    }
    sendMessage(update, text.toString());
  }

  private void sendMessage(Update update, String text) {
    SendMessage message = new SendMessage();
    message.setChatId(String.valueOf(update.getMessage().getChatId()));
    message.setText(StringUtils.right(text, 4096));
    try {
      execute(message); // Call method to send the message
    } catch (TelegramApiException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getBotUsername() {
    return configuration.getUsername();
  }

  @Override
  public String getBotToken() {
    return configuration.getToken();
  }

}

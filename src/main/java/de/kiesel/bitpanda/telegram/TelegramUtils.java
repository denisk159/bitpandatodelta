package de.kiesel.bitpanda.telegram;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class TelegramUtils {

  enum MessageType {
    PICTURE,
    TEXT;
  }

  public static void sendDebugMessage(String text) {
    sendMessageInternal(text, TelegramConfiguration.DEBUG);
  }

  public static void sendMessage(String text) {
    sendMessageInternal(text, TelegramConfiguration.CRYPTO);
  }

  private static void sendMessageInternal(String text, TelegramConfiguration telegramConfiguration) {
    var message = new SendMessage();
    message.setChatId(telegramConfiguration.getChatId());
    message.setText(text);
    message.enableMarkdownV2(true);
    message.enableMarkdown(true);
    message.enableHtml(true);
    try {
      new TelegramBot(telegramConfiguration).execute(message);
    } catch (TelegramApiException e) {
      e.printStackTrace();
    }
  }

  static void log(String error) {
    sendDebugMessage(error);
  }
}

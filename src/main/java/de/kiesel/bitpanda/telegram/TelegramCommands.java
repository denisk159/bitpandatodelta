package de.kiesel.bitpanda.telegram;

public enum TelegramCommands {
  HELP("help", "Listet alle verfügbaren Befehle auf."),
  LOGS("logs", "Zeigt die letzten Log-Einträge an."),
  NEW("new", "Zeigt die letzten Produkte von YT.");

  private final String command;
  private final String description;

  TelegramCommands(String command, String description) {
    this.command = command;
    this.description = description;
  }

  public String getCommand() {
    return command;
  }

  public static TelegramCommands getByCommand(String command) {
    if (command.startsWith("/")) {
      command = command.replace("/", "");
    }
    for (TelegramCommands telegramCommands : TelegramCommands.values()) {
      if (telegramCommands.getCommand().equals(command)) {
        return telegramCommands;
      }
    }
    return null;
  }

  public String getDescription() {
    return description;
  }
}

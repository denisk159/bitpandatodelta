package de.kiesel.bitpanda.telegram;

public enum TelegramConfiguration {
  CRYPTO(System.getenv("TelegramCryptoChatId"), System.getenv("TelegramCryptoToken"), "crypto"),
  DEBUG(System.getenv("TelegramDebugChatId"), System.getenv("TelegramDebugToken"), "yt-debug");

  private final String chatId;
  private final String token;
  private final String username;

  TelegramConfiguration(String chatId, String token, String username) {
    this.chatId = chatId;
    this.token = token;
    this.username = username;
  }

  public String getChatId() {
    return chatId;
  }

  public String getToken() {
    return token;
  }

  public String getUsername() {
    return username;
  }

}


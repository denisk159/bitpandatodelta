package de.kiesel.bitpanda;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Configuration
@EnableWebMvc
@EnableScheduling
public class AppConfig implements WebMvcConfigurer {
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    String currentPath = new File(".").getAbsolutePath();
    String location = "file:///" + currentPath + "/WebContent/";
    registry
      .addResourceHandler("/**")
      .addResourceLocations(location);
  }
}

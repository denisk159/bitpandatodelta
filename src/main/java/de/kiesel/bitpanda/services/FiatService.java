package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.Fiat;
import de.kiesel.bitpanda.repository.FiatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class FiatService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  FiatRepository fiatRepository;

  private static final String SELECT_ALL = "SELECT " + Fiat.ID + ", " + Fiat.SYMBOL + " FROM " + Fiat.TABLE;

  public Fiat createData(Fiat fiat) {
    if (!fiatRepository.existsById(fiat.getId())) {
      jdbcTemplate.execute(String.format("INSERT INTO fiat (" + Fiat.ID + ", " + Fiat.SYMBOL + ") VALUES ('%s', '%s');", fiat.getId(), fiat.getSymbol()));
    }
    return fiatRepository.findById(fiat.getId()).orElse(null);
  }

  public Fiat getByFiatId(Long fiatId) {
    List<Fiat> fiatList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Fiat.ID + " = ?", (rs, rowNum) -> new Fiat(rs), fiatId);
    if (fiatList.size() == 1) {
      return fiatList.get(0);
    }
    return null;
  }

  public Fiat getBySymbol(String symbol) {
    List<Fiat> fiatList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Fiat.SYMBOL + " = ?", (rs, rowNum) -> new Fiat(rs), symbol);
    if (fiatList.size() == 1) {
      return fiatList.get(0);
    }
    return null;
  }

}

package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.Depot;
import de.kiesel.bitpanda.enums.DepotType;
import de.kiesel.bitpanda.repository.DeportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;

@Service
public class DepotService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  private DeportRepository deportRepository;

  private static final String SELECT_ALL = "SELECT " + Depot.ID + ", " + Depot.TYPE + ", " + Depot.NAME + ", " + Depot.PROFILE + ", " + Depot.ADDRESS + ", " + Depot.SECRET + ", " + Depot.API + " FROM " + Depot.TABLE;

  public Depot createData(Depot depot) {
    if (depot.getId() == null || !deportRepository.existsById(depot.getId())) {
      jdbcTemplate.execute(String.format("INSERT INTO " + Depot.TABLE + " (" + Depot.TYPE + ", " + Depot.NAME + ", " + Depot.PROFILE + ", " + Depot.ADDRESS + ", " + Depot.SECRET + ", " + Depot.API + ") VALUES ('%s', %s, %s, %s, %s, %s, %s);", depot.getType(), depot.getName(), depot.getProfileId(), depot.getAddress(), depot.getSecret(), depot.getApi()));
    }
    return getById(depot.getId());
  }

  public Depot getById(Long id) {
    List<Depot> depotList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Depot.ID + " = ?", (rs, rowNum) -> new Depot(rs), id);
    if (depotList.isEmpty()) {
      return null;
    }
    return depotList.get(0);
  }

  public Depot getBy(Long profileId, DepotType type){
    List<Depot> depotList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Depot.PROFILE + " = ? AND " + Depot.TYPE + " = ?", (rs, rowNum) -> new Depot(rs), profileId, type.getIdentifier());
    if (depotList.isEmpty()) {
      return null;
    }
    return depotList.get(0);
  }

  public List<Depot> getByProfile(Long profileId){
    List<Depot> depotList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Depot.PROFILE + " = ?", (rs, rowNum) -> new Depot(rs), profileId);
    return depotList;
  }

  public Collection<Depot> getAll() {
    return (Collection<Depot>) deportRepository.findAll();
  }

}

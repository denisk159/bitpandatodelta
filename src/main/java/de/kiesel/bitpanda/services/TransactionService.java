package de.kiesel.bitpanda.services;

import com.litesoftwares.coingecko.CoinGeckoApiClient;
import com.litesoftwares.coingecko.constant.Currency;
import com.litesoftwares.coingecko.impl.CoinGeckoApiClientImpl;
import de.kiesel.bitpanda.controller.Asset;
import de.kiesel.bitpanda.db.Depot;
import de.kiesel.bitpanda.db.Profile;
import de.kiesel.bitpanda.db.Transaction;
import de.kiesel.bitpanda.enums.DepotType;
import de.kiesel.bitpanda.repository.TransactionRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TransactionService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  TransactionRepository transactionRepository;

  @Autowired
  CurrencyService currencyService;

  private static final String ALIAS = "t";
  private static final String COLUMN_PREFIX = ALIAS + ".";
  private static final String ALL_COLUMNS = Transaction.DATE +
    ", " +
    Transaction.AMOUNT_IN +
    ", " +
    Transaction.CURRENCY_IN +
    ", " +
    Transaction.AMOUNT_OUT +
    ", " +
    Transaction.CURRENCY_OUT +
    ", " +
    Transaction.AMOUNT_FEE +
    ", " +
    Transaction.CURRENCY_FEE +
    ", " +
    Transaction.TYPE +
    ", " +
    Transaction.PRICE_CURRENCY_IN +
    ", " +
    Transaction.HASH +
    ", " +
    Transaction.DEPOT_ID;

  private static final String ALL_COLUMNS_FOR_JOIN = COLUMN_PREFIX + Transaction.DATE +
    ", " +
    COLUMN_PREFIX + Transaction.AMOUNT_IN +
    ", " +
    COLUMN_PREFIX + Transaction.CURRENCY_IN +
    ", " +
    COLUMN_PREFIX + Transaction.AMOUNT_OUT +
    ", " +
    COLUMN_PREFIX + Transaction.CURRENCY_OUT +
    ", " +
    COLUMN_PREFIX + Transaction.AMOUNT_FEE +
    ", " +
    COLUMN_PREFIX + Transaction.CURRENCY_FEE +
    ", " +
    COLUMN_PREFIX + Transaction.TYPE +
    ", " +
    COLUMN_PREFIX + Transaction.PRICE_CURRENCY_IN +
    ", " +
    COLUMN_PREFIX + Transaction.HASH +
    ", " +
    COLUMN_PREFIX + Transaction.DEPOT_ID;
  private static final String ALL_COLUMNS_W_ID = COLUMN_PREFIX + Transaction.ID + ", " + ALL_COLUMNS_FOR_JOIN;

  private static final String SELECT_ALL = "SELECT " + ALL_COLUMNS_W_ID + " FROM " + Transaction.TABLE + " " + ALIAS;

  public void createData(Transaction transaction) {
    if (transaction.getId() == null || !transactionRepository.existsById(transaction.getId())) {
      jdbcTemplate.execute(String.format(
        "INSERT INTO transaction (" + ALL_COLUMNS + ") VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
        transaction.getDate(),
        transaction.getAmountIn(),
        StringUtils.trimToEmpty(transaction.getCurrencyIn()),
        transaction.getAmountOut(),
        StringUtils.trimToEmpty(transaction.getCurrencyOut()),
        transaction.getAmountFee(),
        transaction.getCurrencyFee(),
        transaction.getType(),
        transaction.getPriceCurrencyIn(),
        transaction.getHash(),
        transaction.getDepotId()));
    }
  }

  public Transaction getByUrl(Long depotId, String hash) {
    List<Transaction> transactionList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Transaction.HASH + " = ? AND " + Transaction.DEPOT_ID + " = ?", (rs, rowNum) -> new Transaction(rs), depotId, hash);
    if (!transactionList.isEmpty()) {
      return transactionList.get(0);
    }
    return null;
  }

  public List<Transaction> getTransactions(Long profileId) {
    var sql = SELECT_ALL;
    sql += " LEFT JOIN " + Depot.TABLE + " d on d.id = t." + Transaction.DEPOT_ID;
    if (profileId != null) {
      sql += " WHERE d." + Depot.PROFILE + " = ?";
    }
    sql += " ORDER BY " + COLUMN_PREFIX + Transaction.DATE + " ASC ";

    return jdbcTemplate.query(sql, (rs, rowNum) -> new Transaction(rs), profileId);
  }

  public List<Transaction> getTransactionsOfDepot(Integer depotId) {
    return jdbcTemplate.query(
      SELECT_ALL + " WHERE " + Transaction.DEPOT_ID + " = ?  ORDER BY " + Transaction.DATE + " ASC ",
      (rs, rowNum) -> new Transaction(rs),
      depotId);
  }

  public Map<String, List<Transaction>> getTransactionsByAsset(Profile profile) {
    var groupByAsset = new HashMap<String, List<Transaction>>();
    var transactions = getTransactions(profile.getId());
    for (Transaction transaction : transactions) {
      groupByAsset.putIfAbsent(transaction.getCurrencyIn(), new ArrayList<>());
      groupByAsset.putIfAbsent(transaction.getCurrencyOut(), new ArrayList<>());
      groupByAsset.get(transaction.getCurrencyIn()).add(transaction);
      groupByAsset.get(transaction.getCurrencyOut()).add(transaction);
    }
    return groupByAsset;
  }

  public List<Asset> getAssets(Profile profile) {
    List<Asset> assets = new ArrayList<>();
    var currentPrices = getPrices();
    var groupByAssets = getTransactionsByAsset(profile);
    for (var entry : groupByAssets.entrySet()) {
      Asset asset = new Asset(entry.getKey());
      if (StringUtils.isBlank(asset.getSymbol())) {
        continue;
      }
      var transactions = entry.getValue();
      boolean isFirstTransaction = true;
      for (var transaction : transactions) {
        if (isFirstTransaction) {
          if (!transaction.getCurrencyOut().equals(asset.getSymbol())) {
            System.out.println("Fehler.");
          }
          isFirstTransaction = false;
        }
        var amountChange = 0d;
        if (transaction.getCurrencyIn().equals(asset.getSymbol())) {
          amountChange = (-1) * transaction.getAmountIn();
        } else if (transaction.getCurrencyOut().equals(asset.getSymbol())) {
          amountChange = transaction.getAmountOut();
        } else if (transaction.getCurrencyFee().equals(asset.getSymbol())) {
          amountChange = transaction.getAmountFee();
        }
        asset.setAmount(asset.getAmount() + amountChange);
      }
      var currency = currencyService.getByCode(asset.getSymbol());
      if (currency != null && StringUtils.isNotBlank(currency.getName())) {
        asset.setName(currency.getName());
      } else {
        asset.setName(asset.getSymbol());
      }
      var price = currentPrices.get(asset.getSymbol());
      if (price != null) {
        asset.setValue(price * asset.getAmount());
      } else if (asset.getSymbol().equals("EUR")) {
        asset.setValue(asset.getAmount());
      }
      assets.add(asset);
    }
    return assets;
  }

  private Map<String, Double> getPrices() {
    CoinGeckoApiClient client = new CoinGeckoApiClientImpl();
    var currencies = currencyService.getAll();
    var coingeckoIds = currencies.stream().map(de.kiesel.bitpanda.db.Currency::getCoingeckoId).collect(Collectors.toList());
    var ids = Strings.join(coingeckoIds, ',');
    var priceMap = client.getPrice(ids, Currency.EUR);
    Map<String, Double> prices = new HashMap<>();
    for (de.kiesel.bitpanda.db.Currency currency : currencies) {

      var key = currency.getCoingeckoId();
      if (key != null) {
        var priceMapVals = priceMap.get(key);
        if (priceMapVals != null && !priceMapVals.isEmpty()) {
          var price = priceMapVals.get(Currency.EUR);
          prices.put(currency.getCode(), price);
        }
      }
    }
    return prices;
  }
}

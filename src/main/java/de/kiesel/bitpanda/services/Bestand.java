package de.kiesel.bitpanda.services;

import java.time.LocalDateTime;

public class Bestand {

  private LocalDateTime date;
  private Double menge;
  private Double kurs;

  public Bestand() {
  }

  public Bestand(LocalDateTime date, Double menge, Double kurs) {
    this.date = date;
    this.menge = menge;
    this.kurs = kurs;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public Double getMenge() {
    return menge;
  }

  public void setMenge(Double menge) {
    this.menge = menge;
  }

  public Double getKurs() {
    return kurs;
  }

  public void setKurs(Double kurs) {
    this.kurs = kurs;
  }

}

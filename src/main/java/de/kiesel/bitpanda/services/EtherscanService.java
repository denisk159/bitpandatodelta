package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.Currency;
import de.kiesel.bitpanda.db.Depot;
import de.kiesel.bitpanda.db.Transaction;
import io.api.etherscan.core.impl.EtherScanApi;
import io.api.etherscan.model.Tx;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static de.kiesel.bitpanda.bitpandaApi.enums.AttributesType.BUY;
import static de.kiesel.bitpanda.utils.ParseUtils.getDocument;

@Service
public class EtherscanService extends ScanService {
  private static final String API_BASE_URL = "https://api.etherscan.io/api";

  @Autowired
  CurrencyService currencyService;

  @Autowired
  FiatService fiatService;

  @Autowired
  private TransactionService transactionService;

  public List<de.kiesel.bitpanda.db.Transaction> checkCryptos(Depot depot) throws InterruptedException {
    if (StringUtils.isBlank(depot.getApi())) {
      return Collections.emptyList();
    }
    List<de.kiesel.bitpanda.db.Transaction> data = new ArrayList<>();
    EtherScanApi api = new EtherScanApi(depot.getApi());

    var txList = api.account().txs(depot.getAddress());
    for (Tx apiTransaction : txList) {
      var transactionInfos = getTransactionInfos(depot.getId(), apiTransaction.getHash());
      if (transactionInfos != null) {
        transactionInfos.setDepotId(depot.getId());
        data.add(transactionInfos);
        Thread.sleep(1000);
      }
    }
    for (var transaction : data) {
      transactionService.createData(transaction);
      Currency currency = currencyService.getByCode(transaction.getCurrencyIn());
      if (currency == null && transaction.getCurrencyIn() != null) {
        currency = new Currency();
        currency.setCode(transaction.getCurrencyIn());
        currencyService.createData(currency);
      }
      currency = currencyService.getByCode(transaction.getCurrencyOut());
      if (currency == null && transaction.getCurrencyOut() != null) {
        currency = new Currency();
        currency.setCode(transaction.getCurrencyOut());
        currencyService.createData(currency);
      }
    }
    return data;

  }

  public de.kiesel.bitpanda.db.Transaction getTransactionInfos(Long depotId, String hash) {
    if (transactionService.getByUrl(depotId, hash) != null) {
      return null;
    }

    return getTransactionInfos(hash);
  }

  public Transaction getTransactionInfos(String hash) {
    var url = "https://etherscan.io/tx/" + hash;
    var doc = getDocument(url);
    if(doc == null){
      return null;
    }
    System.out.println(url);
    var isTransfer = true;
    Element contentBlock = doc.selectFirst("#ContentPlaceHolder1_maintable");
    var rows = contentBlock.select(".row");
    var success = false;
    var transaction = new Transaction();
    Double value = null;
    for (Element row : rows) {
      var firstItalic = row.selectFirst("i");
      if (firstItalic != null) {
        String dataContent = firstItalic.attr("data-content");
        if (dataContent.contains("The status of the transaction.")) {
          String successLabel = row.selectFirst(".u-label").text();
          success = successLabel.equalsIgnoreCase("Success");
        } else if (dataContent.startsWith("Closing price")) {
          var price = row.selectFirst("span").text();
          transaction.setPriceCurrencyIn(Double.parseDouble(price.replace("$", "").replace(",", "").split(" ")[0]));
        } else if (dataContent.contains("Amount paid to the miner for processing the transaction.")) {
          var feeText = row.selectFirst("span[title='Gas Price * Gas Used by Transaction']").text();
          var fees = feeText.split(" ")[0];
          transaction.setAmountFee(Double.parseDouble(fees));
          transaction.setCurrencyFee("ETH");
        } else if (dataContent.startsWith("The date and time at which a transaction is ")) {
          transaction.setDate(getTransactionDate(row));
        } else if (dataContent.startsWith("The sending party of the transaction")) {
          //From
        } else if (dataContent.startsWith("The receiving party of the transaction")) {
          //TO
        } else if (dataContent.startsWith("Highlighted events of the transaction")) {
          isTransfer = false;
        } else if (dataContent.startsWith("The value being transacted in Ether and fiat value.")) {
          var valueText = row.select("span").get(1).text();
          value = Double.parseDouble(valueText.split(" Ether")[0]);
        }
      }
      var firstSpan = row.selectFirst("span");
      if (firstSpan != null && firstSpan.attr("title").contains("Token Transfers")) {
        Elements tokenRows = row.select("#wrapperContent .media-body");
        isTransfer = false;
        String firstName = null;
        for (Element tokenRow : tokenRows) {
          var tokenName = getTokenName(tokenRow);
          var tokenAmount = getTokenAmount(tokenRow);
          if (firstName == null) {
            firstName = tokenName;
          }
          if (tokenName.equals(firstName)) {
            transaction.setCurrencyIn(tokenName);
            transaction.setAmountIn(Optional.ofNullable(transaction.getAmountIn()).orElse(0.0) + tokenAmount);
          } else {
            transaction.setCurrencyOut(tokenName);
            transaction.setAmountOut(Optional.ofNullable(transaction.getAmountOut()).orElse(0.0) + tokenAmount);
          }
        }
      }
      transaction.setType(BUY.getName());
    }
    transaction.setHash(hash);
    if (success) {

      if (StringUtils.isNotBlank(transaction.getCurrencyIn())) {
        return transaction;
      }
      if (value != null && isTransfer) {
        transaction.setAmountOut(value);
        transaction.setCurrencyOut("ETH");
        return transaction;
      }
    }
    return null;
  }

  public static String getResponse(String url) {
    try {
      var obj = new URL(url);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      con.setRequestProperty("User-Agent", "Mozilla/5.0");
      int responseCode = con.getResponseCode();
      if (responseCode == 200) {
        var in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        var response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        return response.toString();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "";
  }
}

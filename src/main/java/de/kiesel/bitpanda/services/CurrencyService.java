package de.kiesel.bitpanda.services;

import com.litesoftwares.coingecko.CoinGeckoApiClient;
import com.litesoftwares.coingecko.domain.Coins.CoinList;
import com.litesoftwares.coingecko.impl.CoinGeckoApiClientImpl;
import de.kiesel.bitpanda.db.Currency;
import de.kiesel.bitpanda.repository.CurrencyRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CurrencyService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  private CurrencyRepository currencyRepository;

  private static final String SELECT_ALL = "SELECT " + Currency.ID + ", " + Currency.SYMBOL + ", " + Currency.BITPANDA_ID + ", " + Currency.COINGECKO_ID + ", " + Currency.NAME + " FROM " + Currency.TABLE;

  public Currency createData(Currency currency) {
    if (currency.getId() == null || !currencyRepository.existsById(currency.getId())) {
      jdbcTemplate.execute(String.format("INSERT INTO currency (" + Currency.SYMBOL + ", " + Currency.BITPANDA_ID + ", " + Currency.COINGECKO_ID + ", " + Currency.NAME + ") VALUES ('%s', %s, %s, %s);", currency.getCode(), currency.getBitpandaId(), currency.getCoingeckoId(), currency.getName()));
    }
    return getByCode(currency.getCode());
  }

  public Currency getByCode(String code) {
    List<Currency> currencyList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Currency.SYMBOL + " = ?", (rs, rowNum) -> new Currency(rs), code);
    if (currencyList.isEmpty()) {
      return null;
    }
    return currencyList.get(0);
  }

  public Currency getByBitpandaId(Long bitpandaId) {
    List<Currency> currencyList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Currency.BITPANDA_ID + " = ?", (rs, rowNum) -> new Currency(rs), bitpandaId);
    if (currencyList.isEmpty()) {
      return null;
    }
    return currencyList.get(0);
  }

  public Collection<Currency> getAll() {
    return (Collection<Currency>) currencyRepository.findAll();
  }

  @Scheduled(fixedDelay = 10000L)
  public void addCoingeckoId() {
    CoinGeckoApiClient client = new CoinGeckoApiClientImpl();
    List<Currency> toCheckList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Currency.COINGECKO_ID + " IS NULL", (rs, rowNum) -> new Currency(rs));
    if(toCheckList.isEmpty()){
      return;
    }
    var coinList = client.getCoinList();
    for (Currency toCheck : toCheckList) {
      var filteredCoinList = coinList.stream().filter(coinList1 -> coinList1.getSymbol().equalsIgnoreCase(toCheck.getCode())).sorted(Comparator.comparingInt(o -> StringUtils.countMatches(o.getId(), '-'))).collect(Collectors.toList());
      if (filteredCoinList.isEmpty()) {
        continue;
      }
      CoinList coin = filteredCoinList.get(0);
      toCheck.setCoingeckoId(coin.getId());
      toCheck.setName(coin.getName());
      currencyRepository.save(toCheck);
    }
  }

  @PostConstruct
  public void addMissingCurrencies(){
    createCurrency(45L, "BSV");
    createCurrency(46L, "BNB");
    createCurrency(193L, "SHIB");
  }

  private void createCurrency(Long bitpandaId, String code){
    Currency currency = getByCode(code);
    if(currency == null){
      currency = new Currency();
      currency.setCode(code);
      currency.setBitpandaId(bitpandaId);
      createData(currency);
    } else if(currency.getBitpandaId() == null || currency.getBitpandaId() == 0){
      currency.setBitpandaId(bitpandaId);
      currencyRepository.save(currency);
    }
  }
}

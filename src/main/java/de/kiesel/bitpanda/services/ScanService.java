package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.utils.ParseUtils;
import org.jsoup.nodes.Element;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ScanService {

  String getTokenName(Element element) {
    var linksOfRow = element.select("a");
    var tokenNameElement = linksOfRow.get(linksOfRow.size() - 1);
    var tokenName = ParseUtils.getSymbol(tokenNameElement.text());
    if (tokenName != null && tokenName.contains("...")) {
      tokenName = tokenNameElement.selectFirst("span").attr("title");
    }
    if ("WBNB".equals(tokenName)) {
      tokenName = "BNB";
    }
    if ("WETH".equals(tokenName)) {
      tokenName = "ETH";
    }
    return tokenName;
  }

  Double getTokenAmount(Element element) {
    var tokenAmountPart = element.select(".mr-1").get(3).text();
    var tokenAmount = tokenAmountPart.split("\\(")[0].trim().replace(",", "");
    return Double.parseDouble(tokenAmount);
  }

  LocalDateTime getTransactionDate(Element element) {
    var clock = element.selectFirst("#clock");
    String datum = clock.parent().text();
    String dateTime = datum.split("\\(")[1].split("\\)")[0];
    return ZonedDateTime.parse(dateTime, DateTimeFormatter.ofPattern("MMM-dd-yyyy hh:mm:ss a '+'z", Locale.ENGLISH)).toLocalDateTime();
  }
}

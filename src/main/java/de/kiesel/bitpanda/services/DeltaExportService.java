package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.csv.delta.DeltaCsv;
import de.kiesel.bitpanda.db.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeltaExportService {

  @Autowired
  private TransactionService transactionService;

  public String createCsv(Profile profile) {
    var transactions = transactionService.getTransactions(profile.getId());

    DeltaCsv<de.kiesel.bitpanda.csv.delta.CsvRow> deltaCsv = new DeltaCsv<>(transactions);
    deltaCsv.write(false);

    return null;
  }

}

package de.kiesel.bitpanda.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.kiesel.bitpanda.bitpandaApi.BitpandaToDelta;
import de.kiesel.bitpanda.bitpandaApi.enums.AttributesType;
import de.kiesel.bitpanda.bitpandaApi.json.fiatWallet.Data;
import de.kiesel.bitpanda.bitpandaApi.json.fiatWallet.Fiatwallet;
import de.kiesel.bitpanda.bitpandaApi.json.fiatWalletTransaction.FiatwalletsTransactions;
import de.kiesel.bitpanda.bitpandaApi.json.trade.BestFeeCollection;
import de.kiesel.bitpanda.bitpandaApi.json.trade.Trade;
import de.kiesel.bitpanda.bitpandaApi.json.wallet.Wallet;
import de.kiesel.bitpanda.db.*;
import de.kiesel.bitpanda.repository.CurrencyRepository;
import de.kiesel.bitpanda.repository.FiatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static de.kiesel.bitpanda.bitpandaApi.BitpandaToDelta.convert;
import static de.kiesel.bitpanda.bitpandaApi.BitpandaToDelta.convertNumber;

@Service
public class BitpandaService {
  private static final String API_BASE_URL = "https://api.bitpanda.com/v1/";
  private static final String TRADES = "trades";
  private static final String WALLETS = "wallets";
  private static final String WALLET_TRANSACTIONS = "wallets/transactions";
  private static final String ASSET_WALLETS = "asset-wallets";
  private static final String ASSET_TRANSACTIONS_COMMODITY = "assets/transactions/commodity";
  private static final String FIAT_WALLETS = "fiatwallets";
  private static final String FIAT_WALLET_TRANSACTIONS = "fiatwallets/transactions";

  @Autowired
  CurrencyService currencyService;

  @Autowired
  FiatService fiatService;

  @Autowired
  BscscanService bscscanService;

  @Autowired
  CurrencyRepository currencyRepository;

  @Autowired
  FiatRepository fiatRepository;

  @Autowired
  TransactionService transactionService;

  public void checkCryptos(Depot depot) {
    checkWaehrungen(depot.getApi());
    checkFiat(depot.getApi());
    checkFiatTransactions(depot);
    var page = 1;
    var pageSize = 100;
    Integer totalCount = null;
    var currentCount = 0;
    Set<String> bestFeeTimestamps = new HashSet<>();
    while (totalCount == null || currentCount < totalCount) {

      var answer = getTrade(page, pageSize, depot.getApi());
      totalCount = answer.getMeta().getTotalCount();
      List<de.kiesel.bitpanda.bitpandaApi.json.trade.Data> datas = answer.getData();
      currentCount += datas.size();
      for (de.kiesel.bitpanda.bitpandaApi.json.trade.Data data : datas) {
        if (transactionService.getByUrl(depot.getId(), data.getId()) != null) {
          continue;
        }
        var transaction = new Transaction();
        var attributes = data.getAttributes();

        var type = AttributesType.getByName(attributes.getType());
        Currency baseCurrency = currencyService.getByBitpandaId(Long.parseLong(attributes.getCryptocoinId()));
        if (baseCurrency == null) {
          System.out.println("Fehlende CryptoCoinId:" + attributes.getCryptocoinId());
        }
        Optional<Fiat> fiatOut = fiatRepository.findById(Long.parseLong(attributes.getFiatId()));
        if (type.equals(AttributesType.BUY)) {
          transaction.setCurrencyOut(baseCurrency == null ? attributes.getCryptocoinId() : baseCurrency.getCode());
          transaction.setAmountOut(convertNumber(attributes.getAmountCryptocoin()));
          transaction.setAmountIn(convertNumber(attributes.getAmountFiat()));
          transaction.setCurrencyIn(fiatOut.map(Fiat::getSymbol).orElse(attributes.getFiatId()));
        } else {
          transaction.setCurrencyIn(baseCurrency == null ? attributes.getCryptocoinId() : baseCurrency.getCode());
          transaction.setAmountIn(convertNumber(attributes.getAmountCryptocoin()));
          transaction.setAmountOut(convertNumber(attributes.getAmountFiat()));
          transaction.setCurrencyOut(fiatOut.map(Fiat::getSymbol).orElse(attributes.getFiatId()));
        }
        if (Boolean.TRUE.equals(attributes.getBfcUsed()) && !bestFeeTimestamps.contains(attributes.getTime().getDateIso8601())) {
          transaction.setAmountFee(getFee(attributes.getBestFeeCollection()));
          bestFeeTimestamps.add(attributes.getTime().getDateIso8601());
        } else {
          transaction.setAmountFee(0d);
        }

        transaction.setDate(convert(attributes.getTime()));
        transaction.setHash(data.getId());
        transaction.setType(type.getName());
        transaction.setPriceCurrencyIn(convertNumber(attributes.getAmountCryptocoin()) / convertNumber(attributes.getPrice()));
        transaction.setCurrencyFee("BEST");
        transaction.setDepotId(depot.getId());
        transactionService.createData(transaction);
      }
      page++;
    }
  }

  private static Double getFee(BestFeeCollection bestFeeCollection) {
    if (bestFeeCollection == null ||
      bestFeeCollection.getAttributes() == null ||
      bestFeeCollection.getAttributes().getWalletTransaction() == null ||
      bestFeeCollection.getAttributes().getWalletTransaction().getAttributes() == null) {
      return null;
    }
    var feeString = bestFeeCollection.getAttributes().getWalletTransaction().getAttributes().getFee();
    return Double.parseDouble(feeString);
  }

  public void checkWaehrungen(String api) {
    String walletsResponse = getResponse(API_BASE_URL + WALLETS, 1, 10000, api);
    var objectMapper = new ObjectMapper();

    try {
      var wallet = objectMapper.readValue(walletsResponse, Wallet.class);
      for (de.kiesel.bitpanda.bitpandaApi.json.wallet.Data walletData : wallet.getData()) {
        var attributes = walletData.getAttributes();
        if (currencyService.getByCode(attributes.getCryptocoinSymbol()) == null) {
          var currency = new Currency();
          currency.setBitpandaId(Long.parseLong(attributes.getCryptocoinId()));
          currency.setCode(attributes.getCryptocoinSymbol());
          currencyService.createData(currency);
        }
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  public void checkFiat(String api) {
    String fiatResponse = getResponse(API_BASE_URL + FIAT_WALLETS, 1, 1000, api);
    var objectMapper = new ObjectMapper();
    try {
      Fiatwallet wallet = objectMapper.readValue(fiatResponse, Fiatwallet.class);
      for (Data walletData : wallet.getData()) {
        var attributes = walletData.getAttributes();
        var fiat = new Fiat();
        fiat.setId(Long.parseLong(attributes.getFiatId()));
        fiat.setSymbol(attributes.getFiatSymbol());
        fiatService.createData(fiat);
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  public void checkFiatTransactions(Depot depot) {
    String fiatResponse = getResponse(API_BASE_URL + FIAT_WALLET_TRANSACTIONS, 1, 1000, depot.getApi());
    var objectMapper = new ObjectMapper();
    try {
      FiatwalletsTransactions wallet = objectMapper.readValue(fiatResponse, FiatwalletsTransactions.class);
      for (de.kiesel.bitpanda.bitpandaApi.json.fiatWalletTransaction.Data walletData : wallet.getData()) {
        var attributes = walletData.getAttributes();
        Transaction transaction = transactionService.getByUrl(depot.getId(), walletData.getId());
        if (transaction != null || (!AttributesType.DEPOSIT.getName().equals(attributes.getType()) && !AttributesType.WITHDRAW.getName().equals(attributes.getType())) || !attributes.getConfirmed()) {
          continue;
        }

        var fiat = fiatService.getByFiatId(Long.parseLong(attributes.getFiatId()));
        transaction = new Transaction();
        transaction.setAmountOut(Double.parseDouble(attributes.getAmount()));
        transaction.setCurrencyOut(fiat.getSymbol());
        transaction.setDate(BitpandaToDelta.convert(attributes.getTime()));
        transaction.setHash(walletData.getId());
        transaction.setCurrencyFee("EUR");
        transaction.setType(AttributesType.DEPOSIT.getName());
        transaction.setAmountFee(Double.parseDouble(attributes.getFee()));
        transaction.setPriceCurrencyIn(1.0);
        transaction.setDepotId(depot.getId());
        transactionService.createData(transaction);
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  public Trade getTrade(int page, int pageSize, String apiKey) {
    String response = getResponse(API_BASE_URL + TRADES, page, pageSize, apiKey);
    try {
      return new ObjectMapper().readValue(response, Trade.class);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String getResponse(String url, int page, int pageSize, String apiKey) {
    try {
      var obj = new URL(url + "?page_size=" + pageSize + "&page=" + page);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
      con.setRequestProperty("X-API-KEY", apiKey);
      con.setRequestProperty("User-Agent", "Mozilla/5.0");
      int responseCode = con.getResponseCode();
      if (responseCode == 200) {
        var in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        var response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        return response.toString();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return "";
  }
}

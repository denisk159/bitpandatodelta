package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.CheckedCurrency;
import de.kiesel.bitpanda.repository.CheckedCurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class CheckedCurrencyService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  private CheckedCurrencyRepository checkedCurrencyRepository;

  private static final String SELECT_ALL = "SELECT " +
      CheckedCurrency.ID +
      ", " +
      CheckedCurrency.ADDED +
      ", " +
      CheckedCurrency.CRYPTO_ID +
      ", " +
      CheckedCurrency.NAME +
      " FROM " +
      CheckedCurrency.TABLE;

  public CheckedCurrency createData(CheckedCurrency checkedCurrency) {
    if (checkedCurrency.getId() == null || !checkedCurrencyRepository.existsById(checkedCurrency.getId())) {
      jdbcTemplate.execute(String.format(
          "INSERT INTO %s (%s, %s, %s) VALUES ('%s', '%s', '%s');",
          CheckedCurrency.TABLE,
          CheckedCurrency.ADDED,
          CheckedCurrency.CRYPTO_ID,
          CheckedCurrency.NAME,
          checkedCurrency.getAdded(),
          checkedCurrency.getCryptoId(),
          checkedCurrency.getName()));
    }
    return getByName(checkedCurrency.getName());
  }

  public CheckedCurrency getByName(String name) {
    List<CheckedCurrency> checkedCurrencyList = jdbcTemplate.query(SELECT_ALL + " WHERE " + CheckedCurrency.NAME + " = ?", (rs, rowNum) -> new CheckedCurrency(rs), name);
    if (checkedCurrencyList.size() == 1) {
      return checkedCurrencyList.get(0);
    }
    return null;
  }

}

package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.csv.cointrackerIo.CointrackerCsv;
import de.kiesel.bitpanda.csv.cointrackerIo.CsvRow;
import de.kiesel.bitpanda.db.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CointrackerExportService {

  @Autowired
  private TransactionService transactionService;

  public String createCsv(Profile profile) {

    var transaction = transactionService.getTransactions(profile.getId());
    CointrackerCsv<CsvRow> cointrackerCsv = new CointrackerCsv<>(transaction);
    cointrackerCsv.write(false);

    return null;
  }

}

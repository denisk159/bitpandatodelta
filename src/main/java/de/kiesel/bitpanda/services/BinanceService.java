package de.kiesel.bitpanda.services;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import de.kiesel.bitpanda.db.Depot;
import de.kiesel.bitpanda.db.Profile;
import de.kiesel.bitpanda.enums.DepotType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BinanceService {

  @Autowired
  TransactionService transactionService;

  @Autowired
  DepotService depotService;

  public void checkCryptos(Depot depot) {
    BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(depot.getApi(), depot.getSecret());
    BinanceApiRestClient client = factory.newRestClient();
    var trades = client.getMyTrades("BNB");
    for (com.binance.api.client.domain.account.Trade trade : trades) {
      String test = "break;";
    }
  }
}

package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.Profile;
import de.kiesel.bitpanda.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileService {
  @Autowired
  DataSource dataSource;

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Autowired
  ProfileRepository profileRepository;

  private static final String ALL_COLUMNS = Profile.IDENTIFIER;
  private static final String ALL_COLUMNS_W_ID = Profile.ID + ", " + ALL_COLUMNS;

  private static final String SELECT_ALL = "SELECT " + ALL_COLUMNS_W_ID + " FROM " + Profile.TABLE;

  public Profile createData(Profile profile) {
    if (!profileRepository.existsById(profile.getId())) {
      jdbcTemplate.execute(String.format(
        "INSERT INTO profile (" + ALL_COLUMNS + ") VALUES ('%s');",
        profile.getIdentifier()));
    }
    return profileRepository.findById(profile.getId()).orElse(null);
  }

  public Profile getByIdentifier(String identifier) {
    List<Profile> profileList = jdbcTemplate.query(SELECT_ALL + " WHERE " + Profile.IDENTIFIER + " = ?", (rs, rowNum) -> new Profile(rs), identifier);
    if (profileList.size() == 1) {
      return profileList.get(0);
    }
    return null;
  }

  public List<Profile> getAll() {
    List<Profile> result = new ArrayList<>();
    profileRepository.findAll().forEach(result::add);

    return result;
  }

}

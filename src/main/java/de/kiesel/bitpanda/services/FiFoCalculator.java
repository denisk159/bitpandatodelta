package de.kiesel.bitpanda.services;

import de.kiesel.bitpanda.db.Transaction;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FiFoCalculator {

  @Autowired
  private TransactionService transactionService;

  private final Map<String, List<Bestand>> auflistung = new HashMap<>();

  private double gewinn = 0d;
  private double gewinnSteuerfrei = 0d;

  public void doCalculate(List<Transaction> transactions) {
    for (Transaction transaction : transactions) {
      Double amountIn = transaction.getAmountIn();
      var currentBestand = auflistung.getOrDefault(transaction.getCurrencyIn(), new ArrayList<>());
      if (currentBestand.isEmpty() && StringUtils.isNotBlank(transaction.getCurrencyIn())) {
        System.out.printf("Fehler: Es gibt keinen Bestand für %s%n", transaction.getCurrencyIn());
        continue;
      }
      var transactionsKursIn = ((transaction.getCurrencyIn().equals("BNB") || transaction.getCurrencyIn().equals("ETH"))
          ? transaction.getPriceCurrencyIn()
          : ((transaction.getPriceCurrencyIn() * transaction.getAmountOut()) / transaction.getAmountIn()));
      for (Bestand bestand : currentBestand) {
        var bestandsMenge = bestand.getMenge() - amountIn;

        System.out.printf("Bestand %s, Menge: %.10f; Kurs: %.10f%n", transaction.getCurrencyIn(), bestand.getMenge(), bestand.getKurs());
        var einkaufsPreis = Math.min(bestand.getMenge(), amountIn) * bestand.getKurs();

        var verkaufsPreis = Math.min(bestand.getMenge(), amountIn) * transactionsKursIn;

        if (bestandsMenge < 0) {
          amountIn = bestandsMenge * -1;
          bestandsMenge = 0;
        }
        bestand.setMenge(bestandsMenge);
        System.out.printf("Einkaufspreis: %.10f (Menge: %.10f * Kurs: %.10f%n", einkaufsPreis, bestandsMenge, bestand.getKurs());
        System.out.printf("Verkaufspreis: %.10f (Menge: %.10f * Kurs: %.10f%n", verkaufsPreis, bestandsMenge, transactionsKursIn);
        System.out.printf("Umsatz: %.10f%n", verkaufsPreis - einkaufsPreis);
        if (bestand.getDate().until(transaction.getDate(), ChronoUnit.YEARS) >= 1) {
          gewinnSteuerfrei += verkaufsPreis - einkaufsPreis;
        } else {
          gewinn += verkaufsPreis - einkaufsPreis;
        }
        if (bestandsMenge > 0) {
          break;
        }
      }
      auflistung.put(transaction.getCurrencyIn(), currentBestand.stream().filter(bestand -> bestand.getMenge() > 0).collect(Collectors.toList()));

      System.out.printf("Transaction %s, Menge: %.10f; Kurs: %.10f%n", transaction.getCurrencyIn(), transaction.getAmountIn(), transaction.getPriceCurrencyIn());
      System.out.println("---");

      var boughtBestand = auflistung.getOrDefault(transaction.getCurrencyOut(), new ArrayList<>());
      if (boughtBestand.isEmpty()) {
        System.out.printf("Kein Bestand für %s vorhanden. Lege neuen Bestand an.%n", transaction.getCurrencyOut());
      } else {
        System.out.printf("Bestand %s, Menge: %.10f; Kurs: %.10f%n", transaction.getCurrencyOut(), boughtBestand.get(0).getMenge(), boughtBestand.get(0).getKurs());
      }
      var transactionsKursOut = ((transaction.getCurrencyOut().equals("BNB") || transaction.getCurrencyOut().equals("ETH"))
          ? transaction.getPriceCurrencyIn()
          : ((transaction.getPriceCurrencyIn() * transaction.getAmountIn()) / transaction.getAmountOut()));
      System.out.printf("Transaction %s, Menge: %.10f; Kurs: %.10f%n", transaction.getCurrencyOut(), transaction.getAmountOut(), transactionsKursOut);
      System.out.println("======");
      boughtBestand.add(new Bestand(transaction.getDate(), transaction.getAmountOut(), transactionsKursOut));
      auflistung.put(transaction.getCurrencyOut(), boughtBestand);
    }
    System.out.printf("Umsatz: %.2f%n", gewinn);
    System.out.printf("UmsatzSteuerfrei: %.2f%n", gewinnSteuerfrei);
  }

}

package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.*;
import java.sql.ResultSet;

@Entity
@Table(name = Profile.TABLE)
public class Profile {

  public static final String TABLE = "profile";

  public static final String ID = "id";
  public static final String IDENTIFIER = "identifier";

  private Long id;
  private String identifier;

  public Profile() {

  }

  public Profile(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setIdentifier(ResultSetUtils.getString(rs, IDENTIFIER));
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }
}

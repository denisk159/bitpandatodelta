package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.*;
import java.sql.ResultSet;
import java.time.LocalDateTime;

@Entity
@Table(name = Transaction.TABLE)
public class Transaction {
  public static final String TABLE = "transaction";

  public static final String ID = "id";
  public static final String DATE = "date";
  public static final String AMOUNT_IN = "amount_in";
  public static final String CURRENCY_IN = "currency_in";
  public static final String AMOUNT_OUT = "amount_out";
  public static final String CURRENCY_OUT = "currency_out";
  public static final String AMOUNT_FEE = "amount_fee";
  public static final String CURRENCY_FEE = "currency_fee";
  public static final String TYPE = "type";
  public static final String PRICE_CURRENCY_IN = "price_currency_in";
  public static final String HASH = "hash";
  public static final String DEPOT_ID = "depot_id";

  private Long id;
  private LocalDateTime date;

  private Double amountIn;
  private String currencyIn;
  private Double amountOut;
  private String currencyOut;
  private Double amountFee;
  private String currencyFee;
  private String type;
  private Double priceCurrencyIn;
  private String hash;
  private Long depotId;

  public Transaction() {
  }

  public Transaction(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setDate(ResultSetUtils.getLocalDateTime(rs, DATE));
    setAmountIn(ResultSetUtils.getDouble(rs, AMOUNT_IN));
    setCurrencyIn(ResultSetUtils.getString(rs, CURRENCY_IN));
    setAmountOut(ResultSetUtils.getDouble(rs, AMOUNT_OUT));
    setCurrencyOut(ResultSetUtils.getString(rs, CURRENCY_OUT));
    setAmountFee(ResultSetUtils.getDouble(rs, AMOUNT_FEE));
    setCurrencyFee(ResultSetUtils.getString(rs, CURRENCY_FEE));
    setType(ResultSetUtils.getString(rs, TYPE));
    setPriceCurrencyIn(ResultSetUtils.getDouble(rs, PRICE_CURRENCY_IN));
    setHash(ResultSetUtils.getString(rs, HASH));
    setDepotId(ResultSetUtils.getLong(rs, DEPOT_ID));
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public Double getAmountIn() {
    return amountIn;
  }

  public void setAmountIn(Double amountIn) {
    this.amountIn = amountIn;
  }

  public String getCurrencyIn() {
    return currencyIn;
  }

  public void setCurrencyIn(String currencyIn) {
    this.currencyIn = currencyIn;
  }

  public Double getAmountOut() {
    return amountOut;
  }

  public void setAmountOut(Double amountOut) {
    this.amountOut = amountOut;
  }

  public String getCurrencyOut() {
    return currencyOut;
  }

  public void setCurrencyOut(String currencyOut) {
    this.currencyOut = currencyOut;
  }

  public Double getAmountFee() {
    return amountFee;
  }

  public void setAmountFee(Double amountFee) {
    this.amountFee = amountFee;
  }

  public String getCurrencyFee() {
    return currencyFee;
  }

  public void setCurrencyFee(String currencyFee) {
    this.currencyFee = currencyFee;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Double getPriceCurrencyIn() {
    return priceCurrencyIn;
  }

  public void setPriceCurrencyIn(Double priceCurrencyIn) {
    this.priceCurrencyIn = priceCurrencyIn;
  }

  public Long getDepotId() {
    return depotId;
  }

  public void setDepotId(Long depotId) {
    this.depotId = depotId;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }
}

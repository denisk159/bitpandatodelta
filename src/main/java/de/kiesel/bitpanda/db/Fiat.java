package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.ResultSet;

@Entity
@Table(name = Fiat.TABLE)
public class Fiat {
  public static final String TABLE = "fiat";

  public static final String ID = "id";
  public static final String SYMBOL = "symbol";

  private Long id;
  private String symbol;

  public Fiat() {

  }

  public Fiat(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setSymbol(ResultSetUtils.getString(rs, SYMBOL));
  }

  @Id
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }
}

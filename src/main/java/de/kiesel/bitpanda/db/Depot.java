package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.*;
import java.sql.ResultSet;

@Entity
@Table(name = Depot.TABLE)
public class Depot {
  public static final String TABLE = "depot";

  public static final String ID = "id";
  public static final String TYPE = "type";
  public static final String NAME = "name";
  public static final String PROFILE = "profile_id";
  public static final String ADDRESS = "address";
  public static final String SECRET = "secret";
  public static final String API = "api";

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private Integer type;
  private String name;
  private Long profileId;
  private String address;
  private String secret;
  private String api;

  public Depot() {

  }

  public Depot(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setType(ResultSetUtils.getInteger(rs, TYPE));
    setName(ResultSetUtils.getString(rs, NAME));
    setProfileId(ResultSetUtils.getLong(rs, PROFILE));
    setAddress(ResultSetUtils.getString(rs, ADDRESS));
    setSecret(ResultSetUtils.getString(rs, SECRET));
    setApi(ResultSetUtils.getString(rs, API));
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getProfileId() {
    return profileId;
  }

  public void setProfileId(Long profileId) {
    this.profileId = profileId;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public String getApi() {
    return api;
  }

  public void setApi(String api) {
    this.api = api;
  }
}

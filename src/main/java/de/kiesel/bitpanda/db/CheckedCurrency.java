package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.*;
import java.sql.ResultSet;
import java.time.LocalDateTime;

@Entity
@Table(name = CheckedCurrency.TABLE)
public class CheckedCurrency {
  public static final String TABLE = "checked_currency";

  public static final String ID = "id";
  public static final String ADDED = "added";
  public static final String CRYPTO_ID = "crypto_id";
  public static final String NAME = "name";

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private LocalDateTime added;
  private Long cryptoId;
  private String name;

  public CheckedCurrency() {

  }

  public CheckedCurrency(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setAdded(ResultSetUtils.getLocalDateTime(rs, ADDED));
    setCryptoId(ResultSetUtils.getLong(rs, CRYPTO_ID));
    setName(ResultSetUtils.getString(rs, NAME));
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getAdded() {
    return added;
  }

  public void setAdded(LocalDateTime added) {
    this.added = added;
  }

  public Long getCryptoId() {
    return cryptoId;
  }

  public void setCryptoId(Long cryptoId) {
    this.cryptoId = cryptoId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

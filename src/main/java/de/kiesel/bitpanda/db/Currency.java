package de.kiesel.bitpanda.db;

import de.kiesel.bitpanda.utils.ResultSetUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.ResultSet;

@Entity
@Table(name = Currency.TABLE)
public class Currency {
  public static final String TABLE = "currency";

  public static final String ID = "id";
  public static final String SYMBOL = "code";
  public static final String BITPANDA_ID = "bitpanda_id";
  public static final String COINGECKO_ID = "coingecko_id";
  public static final String NAME = "name";

  private Long id;
  private String code;
  private Long bitpandaId;
  private String coingeckoId;
  private String name;

  public Currency() {

  }

  public Currency(ResultSet rs) {
    setId(ResultSetUtils.getLong(rs, ID));
    setCode(ResultSetUtils.getString(rs, SYMBOL));
    setBitpandaId(ResultSetUtils.getLong(rs, BITPANDA_ID));
    setCoingeckoId(ResultSetUtils.getString(rs, COINGECKO_ID));
    setName(ResultSetUtils.getString(rs, NAME));
  }

  @Id
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Long getBitpandaId() {
    return bitpandaId;
  }

  public void setBitpandaId(Long bitpandaId) {
    this.bitpandaId = bitpandaId;
  }

  public String getCoingeckoId() {
    return coingeckoId;
  }

  public void setCoingeckoId(String coingeckoId) {
    this.coingeckoId = coingeckoId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

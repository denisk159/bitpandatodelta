package de.kiesel.bitpanda.utils;

public class DeltaRequest {
  private String bitpandaApiKey;
  private String bscApiKey;
  private String trustwalletAddress;
  private Long profileId;

  public DeltaRequest(String bitpandaApiKey) {
    this.bitpandaApiKey = bitpandaApiKey;
  }

  public String getTrustwalletAddress() {
    return trustwalletAddress;
  }

  public void setTrustwalletAddress(String trustwalletAddress) {
    this.trustwalletAddress = trustwalletAddress;
  }

  public String getBscApiKey() {
    return bscApiKey;
  }

  public void setBscApiKey(String bscApiKey) {
    this.bscApiKey = bscApiKey;
  }

  public String getBitpandaApiKey() {
    return bitpandaApiKey;
  }

  public void setBitpandaApiKey(String bitpandaApiKey) {
    this.bitpandaApiKey = bitpandaApiKey;
  }

  public void setProfileId(Long profileId) {
    this.profileId = profileId;
  }

  public Long getProfileId() {
    return profileId;
  }
}

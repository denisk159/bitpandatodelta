package de.kiesel.bitpanda.utils;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.regex.Pattern;

public class ParseUtils {

  private static final String symbolRegex = "\\((.*?)\\)";
  private static final Pattern symbolPattern = Pattern.compile(symbolRegex, Pattern.MULTILINE);

  public static Document getDocument(String detailUrl) {
    Document doc = null;

    var connection = Jsoup.connect(detailUrl);
    try {
      doc = connection.get();
      if (doc == null) {
        throw new IOException();
      }
    } catch (IOException e) {
      e.printStackTrace();
      // TelegramUtils.sendDebugMessage("Fehler beim Abrufen der URL " + detailUrl + "\n" + Arrays.toString(e.getStackTrace()));
    }
    return doc;
  }

  static String getAttribute(Element element, String selFirst, String attr) {
    var el = element.selectFirst(selFirst);
    return el == null ? null : el.attr(attr);
  }

  static String getText(Element element, String selFirst) {
    var el = element.selectFirst(selFirst);
    return el != null ? el.text() : null;
  }

  public static String getSymbol(String text) {
    var matcher = symbolPattern.matcher(text);
    if (StringUtils.isBlank(text) || !matcher.find()) {
      return null;
    }
    return matcher.group(1);
  }

}

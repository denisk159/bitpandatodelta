package de.kiesel.bitpanda.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Calendar;

public class ResultSetUtils {

  public static Long getLong(ResultSet rs, String columnName) {
    try {
      return rs.getLong(columnName);
    } catch (SQLException ignore) {
    }
    return null;
  }

  public static Integer getInteger(ResultSet rs, String columnName) {
    try {
      return rs.getInt(columnName);
    } catch (SQLException ignore) {
    }
    return null;
  }

  public static Double getDouble(ResultSet rs, String columnName) {
    try {
      return rs.getDouble(columnName);
    } catch (SQLException ignore) {
    }
    return null;
  }

  public static String getString(ResultSet rs, String columnName) {
    try {
      return rs.getString(columnName);
    } catch (SQLException ignore) {
    }
    return null;
  }

  public static LocalDateTime getLocalDateTime(ResultSet rs, String columnName) {
    try {
      var calendar = Calendar.getInstance();
      var date = rs.getDate(columnName, calendar);
      var time = rs.getTime(columnName, calendar);
      return LocalDateTime.of(date.toLocalDate(), time.toLocalTime());
    } catch (SQLException throwables) {
    }
    return null;
  }
}

package de.kiesel.bitpanda.csv.parent;

import java.time.LocalDateTime;
import java.util.*;

public abstract class Row<T extends Enum<T>> {
  private final Map<T, Object> rowEntries = new HashMap<>();

  @Override
  public String toString() {
    return "Row{" + "rowEntries=" + rowEntries + '}';
  }

  public Map<T, Object> getRowEntries() {
    return rowEntries;
  }

  public List<T> enumValues(Class<T> enumType) {
    return Arrays.asList(enumType.getEnumConstants().clone());
  }

  public List<String> getColumnValues(Class<T> enumType) {
    List<String> values = new ArrayList<>();
    for (T e : enumValues(enumType)) {
      values.add(getStringValue(e));
    }
    return values;
  }

  protected String getStringValue(T column) {
    var value = getRowEntries().get(column);
    if (value instanceof String) {
      return (String) value;
    } else if (value instanceof Double) {
      return String.valueOf((double) value);
    } else if (value instanceof LocalDateTime) {
      return formatLocalDateTime((LocalDateTime) value);
    } else if (value instanceof Enum) {
      return ((Enum<?>) value).name();
    } else {
      return String.valueOf(value == null ? "" : value);
    }
  }

  protected abstract String formatLocalDateTime(LocalDateTime localDateTime);

}

package de.kiesel.bitpanda.csv.parent;

import de.kiesel.bitpanda.db.Transaction;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public abstract class Csv<T extends Row> {

  private final List<Transaction> transactions;
  private final List<T> allRows = new ArrayList<>();

  protected Csv(List<Transaction> transactions) {
    this.transactions = transactions;
  }

  protected abstract void write(boolean append);

  protected abstract void createRows();

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public List<T> getAllRows() {
    return allRows;
  }

  protected void saveRowsToCsv(List<T> allRows, String[] allColumnNames, String filename, boolean append) {
    try (var fw = new FileWriter(filename, append); StringWriter stringWriter = new StringWriter()) {
      var csvPrinter = new CSVPrinter(stringWriter, append ? CSVFormat.DEFAULT : CSVFormat.DEFAULT.withHeader(allColumnNames));
      for (T row : allRows) {
        csvPrinter.printRecord(getColumnValues(row));
      }
      csvPrinter.flush();
      fw.append(stringWriter.toString());
      fw.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  protected abstract List<String> getColumnValues(T row);

}

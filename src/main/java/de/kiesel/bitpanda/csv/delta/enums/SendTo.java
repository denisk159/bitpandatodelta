package de.kiesel.bitpanda.csv.delta.enums;

public enum SendTo {
  MY_WALLET,
  OTHER_WALLET,
  BANK,
  OTHER
}

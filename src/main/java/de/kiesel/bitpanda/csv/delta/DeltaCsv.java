package de.kiesel.bitpanda.csv.delta;

import de.kiesel.bitpanda.csv.delta.enums.Type;
import de.kiesel.bitpanda.csv.parent.Csv;
import de.kiesel.bitpanda.csv.parent.Row;
import de.kiesel.bitpanda.db.Transaction;

import java.util.Arrays;
import java.util.List;

public class DeltaCsv<T extends Row> extends Csv<CsvRow> {

  public DeltaCsv(List<Transaction> data) {
    super(data);
  }

  private static final List<String> GLOBAL_CRYPTOS = Arrays.asList("65", "64", "45", "46");

  protected void createRows() {
    for (Transaction transaction : getTransactions()) {
      var row = new CsvRow();
      row.setDate(transaction.getDate());
      row.setType(Type.BUY);

      row.setBaseAmount(transaction.getAmountOut());
      row.setBaseCurrency(transaction.getCurrencyOut());
      row.setQuoteAmount(transaction.getAmountIn());
      row.setQuoteCurrency(transaction.getCurrencyIn());

      row.setFee(transaction.getAmountFee());
      row.setFeeCurrency(transaction.getCurrencyFee());
      row.setSyncHoldings(0);
      getAllRows().add(row);
    }

  }

  protected List<String> getColumnValues(CsvRow row) {
    return row.getColumnValues(CsvColumn.class);
  }

  public void write(boolean append) {
    createRows();
    saveRowsToCsv(getAllRows(), CsvColumn.getAllColumnNames(), "deltaFile.csv", append);
  }

}

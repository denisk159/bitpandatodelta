package de.kiesel.bitpanda.csv.delta;

import java.util.Arrays;

public enum CsvColumn {
  DATE("Date"),
  // This field contains the date and time of the transaction, including the timezone. If you don't specify a timezone, the timezone will default to UTC (Coordinated Universal Time)
  TYPE("Type"),
  // The type of the transaction. For trades you can specify 'BUY' or 'SELL', and for transfers you can specify 'DEPOSIT', 'WITHDRAW' or 'TRANSFER'
  EXCHANGE("Exchange"),
  // The exchange where the trade (or deposit/withdraw) was made. Optional for trades. When not specified, we'll use the Global Average to calculate the costs/proceeds and worth on
  BASE_AMOUNT("Base amount"),
  // The amount you were trading or transferring (excluding fees)
  BASE_CURRENCY("Base currency"),
  // The currency you were trading or transferring
  QUOTE_AMOUNT("Quote Amount"),
  // For trades, the amount you were trading it for (excluding fees). It can be calculated as the price per coin * the amount of coins traded
  QUOTE_CURRENCY("Quote currency"),
  // The trade currency
  FEE("Fee"),
  // The fee that was paid on the trade or transfer
  FEE_CURRENCY("Fee currency"),
  // The currency of the fee
  COSTS_PROCEEDS("Costs/Proceeds"),
  // Optional, used for an ICO, we'll take this amount as money invested
  COSTS_PROCEEDS_CURRENCY("Costs/Proceeds currency"),
  // Optional, used for an ICO, the currency of the amount invested
  SYNC_HOLDINGS("Sync holdings"),
  // Optional, for trades. When set to 1, the quote will be added to or deducted from your holdings (depending if it's a SELL or BUY). It's recommended but it can result in a negative balance if you manually entered corresponding trades/transfers.
  SENT_RECEIVED_FROM("Sent/Received from"),
  // In case of an ICO, this field should be 'ICO', otherwise it's only used for transfers. You can specify the name of an exchange, 'MY_WALLET', 'OTHER_WALLET', 'BANK', 'AIRDROP', 'MINING', 'FORK', 'DIVIDENDS' or 'OTHER'
  SENT_TO("Sent to"),
  // Only used for transfers. You can specify the name of an exchange, 'MY_WALLET', 'OTHER_WALLET', 'BANK' or 'OTHER'
  NOTES("Notes");
  // Your notes you want to keep for this transaction (optional)

  private final String columnName;

  CsvColumn(String columnName) {
    this.columnName = columnName;
  }

  public String getColumnName() {
    return columnName;
  }

  public static String[] getAllColumnNames() {
    return Arrays.stream(values()).map(CsvColumn::getColumnName).toArray(String[]::new);
  }
}

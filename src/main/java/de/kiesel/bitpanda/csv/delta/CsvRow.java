package de.kiesel.bitpanda.csv.delta;

import de.kiesel.bitpanda.csv.delta.enums.Type;
import de.kiesel.bitpanda.csv.parent.Row;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CsvRow extends Row<CsvColumn> {

  public void setDate(LocalDateTime date) {
    getRowEntries().put(CsvColumn.DATE, date);
  }

  public void setType(Type type) {
    getRowEntries().put(CsvColumn.TYPE, type);
  }

  public void setExchange(String exchange) {
    getRowEntries().put(CsvColumn.EXCHANGE, exchange);
  }

  public void setBaseAmount(Double baseAmount) {
    getRowEntries().put(CsvColumn.BASE_AMOUNT, baseAmount);
  }

  public void setBaseCurrency(String baseCurrency) {
    getRowEntries().put(CsvColumn.BASE_CURRENCY, baseCurrency);
  }

  public void setQuoteAmount(Double quoteAmount) {
    getRowEntries().put(CsvColumn.QUOTE_AMOUNT, quoteAmount);
  }

  public void setQuoteCurrency(String quoteCurrency) {
    getRowEntries().put(CsvColumn.QUOTE_CURRENCY, quoteCurrency);
  }

  public void setFee(Double fee) {
    getRowEntries().put(CsvColumn.FEE, fee);
  }

  public void setFeeCurrency(String feeCurrency) {
    getRowEntries().put(CsvColumn.FEE_CURRENCY, feeCurrency);
  }

  public void setCostsProceeds(Double costsProceeds) {
    getRowEntries().put(CsvColumn.COSTS_PROCEEDS, costsProceeds);
  }

  public void setCostsProceedsCurrency(String costsProceedsCurrency) {
    getRowEntries().put(CsvColumn.COSTS_PROCEEDS_CURRENCY, costsProceedsCurrency);
  }

  public void setSyncHoldings(Integer syncHoldings) {
    getRowEntries().put(CsvColumn.SYNC_HOLDINGS, syncHoldings);
  }

  public void setSendReceivedFrom(String sendReceivedFrom) {
    getRowEntries().put(CsvColumn.SENT_RECEIVED_FROM, sendReceivedFrom);
  }

  public void setSendTo(String sendTo) {
    getRowEntries().put(CsvColumn.SENT_TO, sendTo);
  }

  public void setNotes(String notes) {
    getRowEntries().put(CsvColumn.NOTES, notes);
  }

  @Override
  protected String formatLocalDateTime(LocalDateTime localDateTime) {
    ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
    String formattedDateTime = zonedDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    formattedDateTime = formattedDateTime.replace("T", " ");
    formattedDateTime = formattedDateTime.replace("+", " +");
    return formattedDateTime;
  }
}

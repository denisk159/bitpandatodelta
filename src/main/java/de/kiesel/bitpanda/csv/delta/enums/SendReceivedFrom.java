package de.kiesel.bitpanda.csv.delta.enums;

public enum SendReceivedFrom {
  // In case of an ICO, this field should be 'ICO', otherwise it's only used for transfers.
  ICO,
  // You can specify the name of an exchange, 'MY_WALLET', 'OTHER_WALLET', 'BANK', 'AIRDROP', 'MINING', 'FORK', 'DIVIDENDS' or 'OTHER'
  MY_WALLET,
  OTHER_WALLET,
  BANK,
  AIRDROP,
  MINING,
  FORK,
  DIVIDENDS,
  OTHER;
}

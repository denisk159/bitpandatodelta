package de.kiesel.bitpanda.csv.cointrackerIo;

import java.util.Arrays;

public enum CsvColumn {
  DATE("Date"),
  // The required date format is MM/DD/YYYY HH:MM:SS (e.g. 09/30/2019 07:19:01)
  RECEIVED_QUANTITY("Received Quantity"),
  RECEIVED_CURRENCY("Received Currency"),
  SENT_QUANTITY("Sent Quantity"),
  SENT_CURRENCY("Sent Currency"),
  FEE_AMOUNT("Fee Amount"),
  FEE_CURRENCY("Fee Currency"),
  TAG("Tag");

  private final String columnName;

  CsvColumn(String columnName) {
    this.columnName = columnName;
  }


  public String getColumnName() {
    return columnName;
  }

  public static String[] getAllColumnNames() {
    return Arrays.stream(values()).map(CsvColumn::getColumnName).toArray(String[]::new);
  }
}

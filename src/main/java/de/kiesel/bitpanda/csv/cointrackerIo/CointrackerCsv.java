package de.kiesel.bitpanda.csv.cointrackerIo;

import de.kiesel.bitpanda.csv.parent.Csv;
import de.kiesel.bitpanda.csv.parent.Row;
import de.kiesel.bitpanda.db.Transaction;

import java.util.List;

public class CointrackerCsv<T extends Row> extends Csv<CsvRow> {

  public CointrackerCsv(List<Transaction> data) {
    super(data);
  }

  public void createRows() {
    for (Transaction transaction : getTransactions()) {
      var row = new CsvRow();

      row.setDate(transaction.getDate());
      row.setReceivedQuantity(transaction.getAmountIn());
      row.setReceivedCurrency(transaction.getCurrencyIn());
      row.setSentQuantity(transaction.getAmountOut());
      row.setSentCurrency(transaction.getCurrencyOut());

      getAllRows().add(row);
    }
  }

  @Override
  protected List<String> getColumnValues(CsvRow row) {
    return row.getColumnValues(CsvColumn.class);
  }

  //  private static void saveRowsToCointrackerCsv(List<csv.cointrackerIo.CsvRow> allRows) {
  //    try (FileWriter fw = new FileWriter("cointrackerIoFile.csv", true); StringWriter stringWriter = new StringWriter()) {
  //      CSVPrinter csvPrinter = new CSVPrinter(stringWriter, CSVFormat.DEFAULT.withHeader(csv.cointrackerIo.CsvColumn.getAllColumnNames()));
  //      for (csv.cointrackerIo.CsvRow row : allRows) {
  //        csvPrinter.printRecord(row.getColumnValues(csv.cointrackerIo.CsvColumn.class));
  //      }
  //      csvPrinter.flush();
  //      fw.append(stringWriter.toString());
  //      fw.flush();
  //    } catch (IOException e) {
  //      e.printStackTrace();
  //    }
  //  }

  public void write(boolean append) {
    createRows();
    saveRowsToCsv(getAllRows(), CsvColumn.getAllColumnNames(), "cointrackerIoFile.csv", append);
  }
}

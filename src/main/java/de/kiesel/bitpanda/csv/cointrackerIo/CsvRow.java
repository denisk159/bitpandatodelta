package de.kiesel.bitpanda.csv.cointrackerIo;

import de.kiesel.bitpanda.csv.parent.Row;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CsvRow extends Row<CsvColumn> {

  @Override
  protected String formatLocalDateTime(LocalDateTime localDateTime) {
    String formattedDateTime = localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
    formattedDateTime = formattedDateTime.replace("T", " ");
    formattedDateTime = formattedDateTime.split("\\+")[0];
    return formattedDateTime;
  }

  public void setDate(LocalDateTime date) {
    getRowEntries().put(CsvColumn.DATE, date);
  }

  public void setReceivedQuantity(Double receivedQuantity) {
    getRowEntries().put(CsvColumn.RECEIVED_QUANTITY, receivedQuantity);
  }

  public void setReceivedCurrency(String receivedCurrency) {
    getRowEntries().put(CsvColumn.RECEIVED_CURRENCY, receivedCurrency);
  }

  public void setSentQuantity(Double sentQuantity) {
    getRowEntries().put(CsvColumn.SENT_QUANTITY, sentQuantity);
  }

  public void setSentCurrency(String sentCurrency) {
    getRowEntries().put(CsvColumn.SENT_CURRENCY, sentCurrency);
  }

  public void setFeeAmount(Double feeAmount) {
    getRowEntries().put(CsvColumn.FEE_AMOUNT, feeAmount);
  }

  public void setFeeCurrency(String feeCurrency) {
    getRowEntries().put(CsvColumn.FEE_CURRENCY, feeCurrency);
  }

  public void setTag(String tag) {
    getRowEntries().put(CsvColumn.TAG, tag);
  }
}

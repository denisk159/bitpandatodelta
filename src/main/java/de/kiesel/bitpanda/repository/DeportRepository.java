package de.kiesel.bitpanda.repository;

import de.kiesel.bitpanda.db.Depot;
import org.springframework.data.repository.CrudRepository;

public interface DeportRepository extends CrudRepository<Depot, Long> {
}

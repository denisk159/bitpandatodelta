package de.kiesel.bitpanda.repository;

import de.kiesel.bitpanda.db.Currency;
import org.springframework.data.repository.CrudRepository;

public interface CurrencyRepository extends CrudRepository<Currency, Long> {
}

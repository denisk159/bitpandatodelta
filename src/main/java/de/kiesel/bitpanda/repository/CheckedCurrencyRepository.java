package de.kiesel.bitpanda.repository;

import de.kiesel.bitpanda.db.CheckedCurrency;
import org.springframework.data.repository.CrudRepository;

public interface CheckedCurrencyRepository extends CrudRepository<CheckedCurrency, Long> {
}

package de.kiesel.bitpanda.repository;

import de.kiesel.bitpanda.db.Profile;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Profile, Long> {
}

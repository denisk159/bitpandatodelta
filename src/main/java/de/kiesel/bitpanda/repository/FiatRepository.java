package de.kiesel.bitpanda.repository;

import de.kiesel.bitpanda.db.Fiat;
import org.springframework.data.repository.CrudRepository;

public interface FiatRepository extends CrudRepository<Fiat, Long> {
}

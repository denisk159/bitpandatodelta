package de.kiesel.bitpanda.enums;

import java.util.Arrays;

public enum DepotType {
  BITPANDA(1, "Bitpanda"),
  BSCSCAN(2, "Bscscan"),
  ETHERSCNA(3, "Etherscan"),
  BINANCE(4, "Binance");

  private final int identifier;
  private final String name;

  DepotType(int identifier, String name) {
    this.identifier = identifier;
    this.name = name;
  }

  public int getIdentifier() {
    return identifier;
  }

  public String getName() {
    return name;
  }

  public static DepotType getByIdentifier(int identifier) {
    return Arrays.stream(DepotType.values()).sequential().filter(at -> at.getIdentifier() == identifier).findFirst().orElse(null);
  }


}

package de.kiesel.bitpanda.controller;

import de.kiesel.bitpanda.db.Depot;
import de.kiesel.bitpanda.db.Profile;
import de.kiesel.bitpanda.db.Transaction;
import de.kiesel.bitpanda.enums.DepotType;
import de.kiesel.bitpanda.services.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;

@Controller
public class RestController {
  @Autowired
  CurrencyService currencyService;

  @Autowired
  BitpandaService bitpandaService;

  @Autowired
  BinanceService binanceService;

  @Autowired
  DeltaExportService deltaExportService;

  @Autowired
  BscscanService bscscanService;

  @Autowired
  EtherscanService etherscanService;

  @Autowired
  ProfileService profileService;

  @Autowired
  TransactionService transactionService;

  @Autowired
  DepotService depotService;

  @GetMapping("/transactions")
  public ResponseEntity<String> getTransactions(@RequestParam(value = "identifier") String identifier, HttpServletRequest request, HttpServletResponse response) {
    Profile profile = profileService.getByIdentifier(identifier);
    List<Transaction> transactions = transactionService.getTransactions(profile.getId());
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(new JSONArray(transactions).toString(), httpHeaders, HttpStatus.OK);
  }

  @GetMapping("/assets")
  public ResponseEntity<String> getAssets(
    @RequestParam(value = "identifier") String identifier, HttpServletRequest request, HttpServletResponse response) {
    Profile profile = profileService.getByIdentifier(identifier);
    List<Asset> assets = transactionService.getAssets(profile);
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(new JSONArray(assets).toString(), httpHeaders, HttpStatus.OK);
  }

  @GetMapping("/depots")
  public ResponseEntity<String> getDepots(
    @RequestParam(value = "identifier") String identifier, HttpServletRequest request, HttpServletResponse response) {
    Profile profile = profileService.getByIdentifier(identifier);
    List<Depot> depots = depotService.getByProfile(profile.getId());
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(new JSONArray(depots).toString(), httpHeaders, HttpStatus.OK);
  }

  @GetMapping("/update")
  public ResponseEntity<String> getDepots(
    @RequestParam(value = "depot") Long depotIdentifier, HttpServletRequest request, HttpServletResponse response) throws InterruptedException {
    Depot depot = depotService.getById(depotIdentifier);

    DepotType depotType = DepotType.getByIdentifier(depot.getType());
    switch (depotType){
      case BITPANDA:
        bitpandaService.checkCryptos(depot);
        break;
      case BSCSCAN:
        bscscanService.checkCryptos(depot);
        break;
      case ETHERSCNA:
        etherscanService.checkCryptos(depot);
        break;
      case BINANCE:
        binanceService.checkCryptos(depot);
        break;
    }

    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(new JSONObject(depot).toString(), httpHeaders, HttpStatus.OK);
  }

  @GetMapping("/delta")
  public ResponseEntity<Resource> createDeltaExport(
    @RequestParam(value = "identifier") String identifier, HttpServletRequest request, HttpServletResponse response) throws InterruptedException {
    var profile = profileService.getByIdentifier(identifier);
    var depot = depotService.getBy(profile.getId(), DepotType.BITPANDA);
    if (depot == null || StringUtils.isBlank(depot.getApi())) {
      return null;
    }
    deltaExportService.createCsv(profile);
    var file = new File("deltaFile.csv");
    try {
      var headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=deltaExport.csv");
      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
      headers.add("Pragma", "no-cache");
      headers.add("Expires", "0");
      var path = Paths.get(file.getAbsolutePath());

      var resource = new ByteArrayResource(Files.readAllBytes(path));

      return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(APPLICATION_OCTET_STREAM).body(resource);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  @GetMapping("/transactionsByAsset")
  public ResponseEntity<String> displayTransactionsByAsset(
    @RequestParam(value = "identifier") String identifier, HttpServletRequest request, HttpServletResponse response) {
    var profile = profileService.getByIdentifier(identifier);
    var transactionsByAsset = transactionService.getTransactionsByAsset(profile);
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(new JSONObject(transactionsByAsset).toString(), httpHeaders, HttpStatus.OK);
  }
}

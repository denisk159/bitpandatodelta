package de.kiesel.bitpanda.controller;

import de.kiesel.bitpanda.db.Profile;
import de.kiesel.bitpanda.db.Transaction;
import de.kiesel.bitpanda.services.ProfileService;
import de.kiesel.bitpanda.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AngularController {

  @Autowired
  private TransactionService transactionService;

  @Autowired
  private ProfileService profileService;

  @GetMapping("/transaction")
  public List<Transaction> getAllTransaction() {
    return transactionService.getTransactions(1L);
  }

  @GetMapping("/transaction/{id}")
  public List<Transaction> getTransactionsOf(@PathVariable Long id) {
    return transactionService.getTransactions(id);
  }

  @GetMapping("/profile")
  public List<Profile> getAllProfile() {
    return profileService.getAll();
  }
}

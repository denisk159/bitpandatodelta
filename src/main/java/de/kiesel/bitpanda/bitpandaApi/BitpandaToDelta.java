package de.kiesel.bitpanda.bitpandaApi;

import de.kiesel.bitpanda.bitpandaApi.enums.AttributesType;
import de.kiesel.bitpanda.bitpandaApi.json.trade.Time;
import de.kiesel.bitpanda.csv.delta.enums.Type;
import de.kiesel.bitpanda.db.Currency;
import de.kiesel.bitpanda.db.Fiat;
import org.apache.commons.lang3.math.NumberUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

public class BitpandaToDelta {

  public static Type convert(AttributesType attributesType) {
    switch (attributesType) {
      case BUY:
        return Type.BUY;
      case DEPOSIT:
        return Type.DEPOSIT;
      case SELL:
        return Type.SELL;
      case TRANSFER:
        return Type.TRANSFER;
      case WITHDRAW:
        return Type.WITHDRAW;
      default:
      case FIAT:
        throw new UnsupportedOperationException();
    }
  }

  public static LocalDateTime convert(Time time) {
    if (time == null || time.getDateIso8601() == null) {
      return null;
    }
    try {
      return LocalDateTime.parse(time.getDateIso8601(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    } catch (Exception e) {
      long timestamp = Long.parseLong(time.getUnix()) * 1000;
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone.getDefault().toZoneId());
    }
  }

  public static LocalDateTime convert(de.kiesel.bitpanda.bitpandaApi.json.fiatWalletTransaction.Time lastChanged) {
    if (lastChanged == null || lastChanged.getDateIso8601() == null) {
      return null;
    }
    try {
      return LocalDateTime.parse(lastChanged.getDateIso8601(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    } catch (Exception e) {
      long timestamp = Long.parseLong(lastChanged.getUnix()) * 1000;
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone.getDefault().toZoneId());
    }
  }

  public static Double convertNumber(String amountCryptocoin) {
    return Double.parseDouble(amountCryptocoin);
  }

  public static String getCurrencyByFiatId(String fiatIdString, List<Fiat> fiat) {
    final Long fiatId = NumberUtils.toLong(fiatIdString, Long.MIN_VALUE);
    return fiat.stream().filter(fiat1 -> fiat1.getId().equals(fiatId)).findFirst().map(Fiat::getSymbol).orElse(fiatIdString);
  }

  public static String getCurrencyByCryptoCoinId(String cryptocoinId, List<Currency> cryptos) {
    final Long coinId = NumberUtils.toLong(cryptocoinId, Long.MIN_VALUE);
    return cryptos.stream().filter(currency -> currency.getId().equals(coinId)).findFirst().map(Currency::getCode).orElse(cryptocoinId);
  }
}

package de.kiesel.bitpanda.bitpandaApi.json.assetWallet;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "wallets"})
public class CryptocoinCollectionAttributes {

    @JsonProperty("wallets")
    private List<CryptocoinWallet> cryptocoinWallets = new ArrayList<>();
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("wallets")
    public List<CryptocoinWallet> getWallets() {
        return cryptocoinWallets;
    }

    @JsonProperty("wallets")
    public void setWallets(List<CryptocoinWallet> cryptocoinWallets) {
        this.cryptocoinWallets = cryptocoinWallets;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

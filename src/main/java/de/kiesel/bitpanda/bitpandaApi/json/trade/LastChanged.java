package de.kiesel.bitpanda.bitpandaApi.json.trade;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date_iso8601", "unix"})
public class LastChanged implements Serializable
{

    @JsonProperty("date_iso8601")
    private String dateIso8601;
    @JsonProperty("unix")
    private String unix;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();
  private final static long serialVersionUID = -7975921543206155255L;

    @JsonProperty("date_iso8601")
    public String getDateIso8601() {
        return dateIso8601;
    }

    @JsonProperty("date_iso8601")
    public void setDateIso8601(String dateIso8601) {
        this.dateIso8601 = dateIso8601;
    }

    @JsonProperty("unix")
    public String getUnix() {
        return unix;
    }

    @JsonProperty("unix")
    public void setUnix(String unix) {
        this.unix = unix;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

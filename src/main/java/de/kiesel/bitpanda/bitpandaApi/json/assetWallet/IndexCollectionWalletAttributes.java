package de.kiesel.bitpanda.bitpandaApi.json.assetWallet;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cryptocoin_id", "cryptocoin_symbol", "balance", "is_default",
    "name",
    "pending_transactions_count",
    "deleted",
    "is_index"
})
public class IndexCollectionWalletAttributes {

    @JsonProperty("cryptocoin_id")
    private String cryptocoinId;
    @JsonProperty("cryptocoin_symbol")
    private String cryptocoinSymbol;
    @JsonProperty("balance")
    private String balance;
    @JsonProperty("is_default")
    private Boolean isDefault;
    @JsonProperty("name")
    private String name;
    @JsonProperty("pending_transactions_count")
    private Integer pendingTransactionsCount;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("is_index")
    private Boolean isIndex;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("cryptocoin_id")
    public String getCryptocoinId() {
        return cryptocoinId;
    }

    @JsonProperty("cryptocoin_id")
    public void setCryptocoinId(String cryptocoinId) {
        this.cryptocoinId = cryptocoinId;
    }

    @JsonProperty("cryptocoin_symbol")
    public String getCryptocoinSymbol() {
        return cryptocoinSymbol;
    }

    @JsonProperty("cryptocoin_symbol")
    public void setCryptocoinSymbol(String cryptocoinSymbol) {
        this.cryptocoinSymbol = cryptocoinSymbol;
    }

    @JsonProperty("balance")
    public String getBalance() {
        return balance;
    }

    @JsonProperty("balance")
    public void setBalance(String balance) {
        this.balance = balance;
    }

    @JsonProperty("is_default")
    public Boolean getIsDefault() {
        return isDefault;
    }

    @JsonProperty("is_default")
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("pending_transactions_count")
    public Integer getPendingTransactionsCount() {
        return pendingTransactionsCount;
    }

    @JsonProperty("pending_transactions_count")
    public void setPendingTransactionsCount(Integer pendingTransactionsCount) {
        this.pendingTransactionsCount = pendingTransactionsCount;
    }

    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @JsonProperty("is_index")
    public Boolean getIsIndex() {
        return isIndex;
    }

    @JsonProperty("is_index")
    public void setIsIndex(Boolean isIndex) {
        this.isIndex = isIndex;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package de.kiesel.bitpanda.bitpandaApi.json.assetWallet;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "wallets"})
public class IndexCollectionAttributes {

    @JsonProperty("wallets")
    private List<IndexWallet> wallets = new ArrayList<>();
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("wallets")
    public List<IndexWallet> getWallets() {
        return wallets;
    }

    @JsonProperty("wallets")
    public void setWallets(List<IndexWallet> wallets) {
        this.wallets = wallets;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

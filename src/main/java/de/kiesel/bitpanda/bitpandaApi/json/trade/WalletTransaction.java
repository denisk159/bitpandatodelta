package de.kiesel.bitpanda.bitpandaApi.json.trade;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type", "attributes", "id"
})
public class WalletTransaction implements Serializable
{

    @JsonProperty("type")
    private String type;
    @JsonProperty("attributes")
    private WalletTransactionAttributes attributes;
    @JsonProperty("id")
    private String id;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();
  private final static long serialVersionUID = -7924992805063594950L;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("attributes")
    public WalletTransactionAttributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(WalletTransactionAttributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package de.kiesel.bitpanda.bitpandaApi.json.assetWallet;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "stock", "etf"})
public class Security {

    @JsonProperty("stock")
    private Stock stock;
    @JsonProperty("etf")
    private Etf etf;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("stock")
    public Stock getStock() {
        return stock;
    }

    @JsonProperty("stock")
    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @JsonProperty("etf")
    public Etf getEtf() {
        return etf;
    }

    @JsonProperty("etf")
    public void setEtf(Etf etf) {
        this.etf = etf;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

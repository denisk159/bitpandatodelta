package de.kiesel.bitpanda.bitpandaApi.json.trade;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "amount",
    "recipient",
    "time",
    "confirmations",
    "in_or_out",
    "type",
    "status",
    "amount_eur",
    "wallet_id",
    "confirmation_by",
    "confirmed",
    "cryptocoin_id",
    "last_changed",
    "fee",
    "current_fiat_id",
    "current_fiat_amount",
    "is_metal_storage_fee",
    "tags",
    "public_status",
    "is_bfc"
})
public class WalletTransactionAttributes implements Serializable
{

    @JsonProperty("amount")
    private String amount;
    @JsonProperty("recipient")
    private String recipient;
    @JsonProperty("time")
    private Time time;
    @JsonProperty("confirmations")
    private Integer confirmations;
    @JsonProperty("in_or_out")
    private String inOrOut;
    @JsonProperty("type")
    private String type;
    @JsonProperty("status")
    private String status;
    @JsonProperty("amount_eur")
    private String amountEur;
    @JsonProperty("wallet_id")
    private String walletId;
    @JsonProperty("confirmation_by")
    private String confirmationBy;
    @JsonProperty("confirmed")
    private Boolean confirmed;
    @JsonProperty("cryptocoin_id")
    private String cryptocoinId;
    @JsonProperty("last_changed")
    private LastChanged lastChanged;
    @JsonProperty("fee")
    private String fee;
    @JsonProperty("current_fiat_id")
    private String currentFiatId;
    @JsonProperty("current_fiat_amount")
    private String currentFiatAmount;
    @JsonProperty("is_metal_storage_fee")
    private Boolean isMetalStorageFee;
  @JsonProperty("tags")
  private List<Object> tags = new ArrayList<>();
  @JsonProperty("public_status")
  private String publicStatus;
  @JsonProperty("is_bfc")
  private Boolean isBfc;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();
  private final static long serialVersionUID = 6426401776206538369L;

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("recipient")
    public String getRecipient() {
        return recipient;
    }

    @JsonProperty("recipient")
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @JsonProperty("time")
    public Time getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Time time) {
        this.time = time;
    }

    @JsonProperty("confirmations")
    public Integer getConfirmations() {
        return confirmations;
    }

    @JsonProperty("confirmations")
    public void setConfirmations(Integer confirmations) {
        this.confirmations = confirmations;
    }

    @JsonProperty("in_or_out")
    public String getInOrOut() {
        return inOrOut;
    }

    @JsonProperty("in_or_out")
    public void setInOrOut(String inOrOut) {
        this.inOrOut = inOrOut;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("amount_eur")
    public String getAmountEur() {
        return amountEur;
    }

    @JsonProperty("amount_eur")
    public void setAmountEur(String amountEur) {
        this.amountEur = amountEur;
    }

    @JsonProperty("wallet_id")
    public String getWalletId() {
        return walletId;
    }

    @JsonProperty("wallet_id")
    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    @JsonProperty("confirmation_by")
    public String getConfirmationBy() {
        return confirmationBy;
    }

    @JsonProperty("confirmation_by")
    public void setConfirmationBy(String confirmationBy) {
        this.confirmationBy = confirmationBy;
    }

    @JsonProperty("confirmed")
    public Boolean getConfirmed() {
        return confirmed;
    }

    @JsonProperty("confirmed")
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    @JsonProperty("cryptocoin_id")
    public String getCryptocoinId() {
        return cryptocoinId;
    }

    @JsonProperty("cryptocoin_id")
    public void setCryptocoinId(String cryptocoinId) {
        this.cryptocoinId = cryptocoinId;
    }

    @JsonProperty("last_changed")
    public LastChanged getLastChanged() {
        return lastChanged;
    }

    @JsonProperty("last_changed")
    public void setLastChanged(LastChanged lastChanged) {
        this.lastChanged = lastChanged;
    }

    @JsonProperty("fee")
    public String getFee() {
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(String fee) {
        this.fee = fee;
    }

    @JsonProperty("current_fiat_id")
    public String getCurrentFiatId() {
        return currentFiatId;
    }

    @JsonProperty("current_fiat_id")
    public void setCurrentFiatId(String currentFiatId) {
        this.currentFiatId = currentFiatId;
    }

    @JsonProperty("current_fiat_amount")
    public String getCurrentFiatAmount() {
        return currentFiatAmount;
    }

    @JsonProperty("current_fiat_amount")
    public void setCurrentFiatAmount(String currentFiatAmount) {
        this.currentFiatAmount = currentFiatAmount;
    }

    @JsonProperty("is_metal_storage_fee")
    public Boolean getIsMetalStorageFee() {
        return isMetalStorageFee;
    }

    @JsonProperty("is_metal_storage_fee")
    public void setIsMetalStorageFee(Boolean isMetalStorageFee) {
        this.isMetalStorageFee = isMetalStorageFee;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("public_status")
    public String getPublicStatus() {
        return publicStatus;
    }

    @JsonProperty("public_status")
    public void setPublicStatus(String publicStatus) {
        this.publicStatus = publicStatus;
    }

    @JsonProperty("is_bfc")
    public Boolean getIsBfc() {
        return isBfc;
    }

    @JsonProperty("is_bfc")
    public void setIsBfc(Boolean isBfc) {
        this.isBfc = isBfc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

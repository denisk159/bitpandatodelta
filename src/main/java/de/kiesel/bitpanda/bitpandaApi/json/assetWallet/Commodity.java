package de.kiesel.bitpanda.bitpandaApi.json.assetWallet;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "metal"})
public class Commodity {

    @JsonProperty("metal")
    private Metal metal;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("metal")
    public Metal getMetal() {
        return metal;
    }

    @JsonProperty("metal")
    public void setMetal(Metal metal) {
        this.metal = metal;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

import {Component, OnInit} from '@angular/core';
import {ProfileService} from "./services/profile.service";
import {Profile} from "./model/profile";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'bitpanda';
  profiles: Profile[];
  profileId: number = 1;

  constructor(private profileService: ProfileService) {
  }

  ngOnInit(): void {
    this.profileService.findAllProfiles().subscribe(data => this.profiles = data);
  }
}

import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Transaction} from "../model/transaction";


@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) {

  }

  findTransactions(profileId: number): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`http://localhost:8080/api/transaction/${profileId}`);
  }

  findAllTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`http://localhost:8080/api/transaction`);
  }
}

import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable, of} from "rxjs";
import {Transaction} from "../model/transaction";
import {TransactionService} from "./transaction.service";
import {catchError, finalize} from "rxjs/operators";


export class TransactionDataSource implements DataSource<Transaction> {

  private transactionSubject = new BehaviorSubject<Transaction[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private transactionService: TransactionService) {

  }

  /*
  loadTransactions(profileId: number,
                   filter: string,
                   sortDirection: string,
                   pageIndex: number,
                   pageSize: number) {
    */
  loadTransactions(profileId: number) {

    this.loadingSubject.next(true);

    this.transactionService.findTransactions(profileId).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(profile => this.transactionSubject.next(profile));

  }

  connect(collectionViewer: CollectionViewer): Observable<Transaction[]> {
    console.log("Connecting data source");
    return this.transactionSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.transactionSubject.complete();
    this.loadingSubject.complete();
  }

}

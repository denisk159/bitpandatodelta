import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Profile} from "../model/profile";


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) {

  }

  findProfile(id: number): Observable<Profile> {
    return this.http.get<Profile>(`http://localhost:8080/api/profile/${id}`);
  }

  findAllProfiles(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`http://localhost:8080/api/profile`);
  }
}

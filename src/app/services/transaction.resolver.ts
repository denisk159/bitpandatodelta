import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Transaction} from "../model/transaction";
import {Observable} from "rxjs";
import {TransactionService} from "./transaction.service";


@Injectable({
  providedIn: 'root'
})
export class CourseResolver implements Resolve<Transaction> {

  constructor(private transactionService: TransactionService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Transaction> {
    return this.transactionService.findTransactions(2);
  }

}

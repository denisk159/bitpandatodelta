import {Component, Input, OnInit} from '@angular/core';
import {TransactionDataSource} from "../services/transaction.datasource";
import {TransactionService} from "../services/transaction.service";

@Component({
  selector: 'app-transaction-table',
  templateUrl: './transaction-table.component.html',
  styleUrls: ['./transaction-table.component.scss']
})
export class TransactionTableComponent implements OnInit {

  dataSource: TransactionDataSource;
  displayedColumns: string[] = ['date', 'currency_out', 'amount_out', 'price_currency_in', 'url'];
  @Input() profileId: number;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.dataSource = new TransactionDataSource(this.transactionService);
    console.log(this.profileId);
    this.dataSource.loadTransactions(this.profileId);
  }

}

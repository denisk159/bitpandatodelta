export interface Transaction {
  currencyOut: string;
  date: string;
  amountOut: number;
  priceCurrencyIn: number;
  url: string;
}

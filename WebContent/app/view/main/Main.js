/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('TaxTool.view.main.Main', {
  extend: 'Ext.tab.Panel',
  xtype: 'app-main',

  requires: [
    'Ext.MessageBox',
    'Ext.layout.Fit'
  ],

  controller: 'main',
  viewModel: 'main',

  defaults: {
    tab: {
      iconAlign: 'top'
    }
  },

  tabBarPosition: 'top',

  items: [
    {
      title: 'Dashboard',
      iconCls: 'x-fa fa-home',
      layout: 'fit',
      // The following grid shares a store with the classic version's grid as well!

    }, {
      title: 'Depots',
      iconCls: 'x-fa fa-user',
      layout: 'fit',
      items: [{
        xtype: 'depotList'
      }]
    }, {
      title: 'Assets',
      iconCls: 'x-fa fa-cog',
      layout: 'fit',
      items: [{
        xtype: 'assetList'
      }]
    }, {
      title: 'Transactions',
      iconCls: 'x-fa fa-users',
      layout: 'fit',
      items: [{
        xtype: 'transactionList'
      }]
    }
  ]
});

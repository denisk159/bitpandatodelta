Ext.define('TaxTool.view.asset.List', {
  extend: 'Ext.grid.Grid',
  xtype: 'assetList',

  requires: [
    'TaxTool.store.Assets'
  ],

  title: 'Assets',

  itemConfig: {
    viewModel: true
  },

  store: {
    type: 'assets'
  },

  columns: [{
    text: 'Coin',
    dataIndex: 'name',
    flex: 1
  }, {
    text: 'Menge',
    dataIndex: 'amount',
    flex: 1
  }, {
    text: 'Wert',
    dataIndex: 'value',
    flex: 1,
    cell: {
      bind: '{record.value} EUR'
    }
  }],

  listeners: {
    select: 'onItemSelected'
  }
});

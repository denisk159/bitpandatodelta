Ext.define('TaxTool.view.transaction.List', {
  extend: 'Ext.grid.Grid',
  xtype: 'transactionList',

  requires: [
    'TaxTool.store.Transactions'
  ],

  title: 'Transactions',

  itemConfig: {
    viewModel: true
  },

  store: {
    type: 'transactions'
  },

  columns: [{
    text: 'Date',
    dataIndex: 'date',
    xtype: 'datecolumn',
    flex: 1,
    format:'d.m.Y, H:i',
  }, {
    text: 'Typ',
    dataIndex: 'type',
    flex: 1
  }, {
    text: 'Eingang',
    flex: 1,
    cell: {
      bind: '{record.amountIn} {record.currencyIn}'
    }
  }, {
    text: 'Ausgang',
    flex: 1,
    cell: {
      bind: '{record.amountOut} {record.currencyOut}'
    }
  }, {
    text: 'Gebühren',
    flex: 1,
    cell: {
      bind: '{record.amountFee} {record.currencyFee}'
    }
  }],

  listeners: {
    select: 'onItemSelected'
  }
});

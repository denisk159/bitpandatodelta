Ext.define('TaxTool.view.depot.List', {
  extend: 'Ext.grid.Grid',
  xtype: 'depotList',

  requires: [
    'TaxTool.store.Depots'
  ],

  title: 'Depots',

  itemConfig: {
    viewModel: true
  },

  store: {
    type: 'depots'
  },

  columns: [{
    text: 'Name',
    dataIndex: 'name',
    flex: 1
  }, {
    text: 'Type',
    dataIndex: 'type',
    flex: 1,
    renderer: function (value) {
      if (value == null) {
        return '';
      }
      switch (value) {
        case 1:
          return 'Bitpanda';
        case 2:
          return 'Bscscan';
        case 3:
          return 'Etherscan';
        case 4:
          return 'Binance';
        default:
          return value;
      }
    }
  }, {
    text: 'Value',
    flex: 1,
    cell: {
      tools: {
        // Tools can also be configured using an object.
        refresh: {
          handler: 'onUpdate',
          zone: 'end',
          tooltip: 'Update Transaction...',
        }
      }
    }
  }]
});

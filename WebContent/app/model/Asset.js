Ext.define('TaxTool.model.Asset', {
  extend: 'Ext.data.Model',

  fields: ['symbol', 'name', 'amount', 'value']
});

Ext.define('TaxTool.model.Transaction', {
  extend: 'Ext.data.Model',

  fields: ['id', 'amountIn', 'currencyIn', 'amountOut', 'currencyOut',
    'amountFee', 'currencyFee', 'type', 'priceCurrencyIn', 'url', 'profileId',
    {name: 'date', type: 'date'}]
});

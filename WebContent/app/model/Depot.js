Ext.define('TaxTool.model.Depot', {
  extend: 'Ext.data.Model',

  fields: ['id', 'type', 'name', 'profileId']

});

Ext.define('TaxTool.model.Profile', {
  extend: 'Ext.data.Model',

  fields: [
    'id', 'identifier'
  ]
});

Ext.define('TaxTool.store.Assets', {
  extend: 'Ext.data.Store',

  alias: 'store.assets',

  model: 'TaxTool.model.Asset',
  autoLoad: true,
  proxy: {
    type: 'rest',
    url: '/assets',
    extraParams: {
      identifier: 'denis'
    }
  },
});

Ext.define('TaxTool.store.Depots', {
  extend: 'Ext.data.Store',

  alias: 'store.depots',

  model: 'TaxTool.model.Depot',
  autoLoad: true,
  proxy: {
    type: 'rest',
    url: '/depots',
    extraParams: {
      identifier: 'denis'
    }
  },
});

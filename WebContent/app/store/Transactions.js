Ext.define('TaxTool.store.Transactions', {
  extend: 'Ext.data.Store',

  alias: 'store.transactions',

  model: 'TaxTool.model.Transaction',
  autoLoad: true,
  proxy: {
    type: 'rest',
    url: '/transactions',
    extraParams: {
      identifier: 'denis'
    }
  },
});

/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'TaxTool.Application',

    name: 'TaxTool',

    requires: [
        // This will automatically load all classes in the TaxTool namespace
        // so that application classes do not need to require each other.
        'TaxTool.*'
    ],

    // The name of the initial view to create.
    mainView: 'TaxTool.view.main.Main'
});
